import React, { Component, PropTypes } from 'react';

import requireLocalizations from '../localizations/output/all.js';

// import Loading from './atoms/Loading';
import Login from '../containers/Login';

class LoginPage extends Component {

  constructor(props, context) {
    super(props, context);
    this.l = requireLocalizations(context.locale);
  }

  render() {
    return (
      <div className="wrapper is-dark">
        <img src="../imgs/civicit_logo_secondary.svg" />
        <div className="wrapper-center is-login">
          <Login />
        </div>
      </div>
      );
  }
}

LoginPage.contextTypes = {
  locale: PropTypes.string
};

export default LoginPage;
