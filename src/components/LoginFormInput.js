import React, {Component, PropTypes} from 'react';

import requireLocalizations from '../localizations/output/all.js';

class LoginFormInput extends Component {

  constructor(props, context) {
    super(props, context);
    this.l = requireLocalizations(context.locale);
  }

  render() {
    const props = this.props;
    const l = this.l;

    return (
      <div>
        <input {...props.input} className={props.touched && props.error ? 'is-danger' : null} />
        <div className="error" >
          { props.touched && props.error && l(props.error)}
        </div>
      </div>
    );
  }
}

LoginFormInput.contextTypes = {
  locale: PropTypes.string
};

export default LoginFormInput;