import React from 'react';
import sinon from 'sinon';
import {mount, shallow} from 'enzyme';
import {expect} from 'chai';

import requireLocalizations from '../localizations/output/all.js';

import MainMenu from './MainMenu';

const mountSimple = () => {
  return mount(<MainMenu />);
}
const shallowSimple = () => {
  return shallow(<MainMenu />);
}

describe('<MainMenu />', () => {

  let wrapper;

  it('should call componentDidMount just once', () => {
    sinon.spy(MainMenu.prototype, 'componentDidMount');
    wrapper = mountSimple();

    expect(MainMenu.prototype.componentDidMount.calledOnce).to.equal(true);
  });

  it('should have own class ', () => {
    wrapper = shallowSimple();
    expect(wrapper.is('.main-menu')).to.equal(true);
    expect(wrapper.find('.main-menu')).to.be.length(1);
  });

});
