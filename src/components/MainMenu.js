import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';

import Icon from './atoms/Icon';
import NavLink from './atoms/NavLink';

import requireLocalizations from '../localizations/output/all.js';

class MainMenu extends Component {

  constructor(props, context) {
    super(props, context);
    this.l = requireLocalizations(context.locale);
  }

  componentDidMount() {}

  render() {
    const l = this.l;
    return (
      <div className="main-menu">

        <div className="main-menu__logo">
          <img src="/imgs/civicit_logo_secondary.svg" />
          <img className="logo-label" src="/imgs/label_admin.svg" />
        </div>

        <ul className="accordion-menu">
            <li className="main-menu__item">
              <NavLink to={`/admin/settings`}>
                <Icon kind="icon_user_gotoprofile" size={24} color="#FFF" />
                <span>{l('MAIN_MENU->ACCOUNT')}</span>
              </NavLink>
            </li>
            <li className="main-menu__item">
              <NavLink to={`/admin/users`}>
                <Icon kind="icon_user_directory" size={24} color="#FFF" />
                <span>{l('MAIN_MENU->USER_DIRECTORY')}</span>
              </NavLink>
            </li>
            <li className="main-menu__item">
              <NavLink to={`/admin/moderation`}>
                <Icon kind="icon_flagged_content" size={24} color="#FFF" />
                <span>{l('MAIN_MENU->MODERATION')}</span>
              </NavLink>
            </li>
            <li className="main-menu__item">
              <NavLink to={`/admin/profiles`}>
                <Icon kind="icon_business_profiles" size={24} color="#FFF" />
                <span>{l('MAIN_MENU->BUSINESS_PROFILES')}</span>
              </NavLink>
            </li>
            <li className="main-menu__item">
              <NavLink to={`/admin/polls`}>
                <Icon kind="icon_advanced_polls" size={24} color="#FFF" />
                <span>{l('MAIN_MENU->ADV_POLLS')}</span>
              </NavLink>
            </li>
            <li className="main-menu__item">
              <NavLink to={`/admin/consultation`}>
                <Icon kind="icon_vote" size={24} color="#FFF" />
                <span>{l('MAIN_MENU->CONSULTATIONS')}</span>
              </NavLink>
            </li>
            <li className="main-menu__item">
              <NavLink to={`/admin/sentiment`}>
                <Icon kind="icon_sentiment_analysis" size={24} color="#FFF" />
                <span>{l('MAIN_MENU->SENTIMENT_ANALYSIS')}</span>
              </NavLink>
            </li>
            <li className="main-menu__item">
              <NavLink to={`/admin/audit`}>
                <Icon kind="icon_moderation_audit" size={24} color="#FFF" />
                <span>{l('MAIN_MENU->MODERATION_AUDIT')}</span>
              </NavLink>
            </li>
            <li className="main-menu__item">
              <NavLink to={`/admin/validation`}>
                <Icon kind="icon_user_ok" size={24} color="#FFF" />
                <span>{l('MAIN_MENU->ADV_USER_VALIDATION')}</span>
              </NavLink>
            </li>
        </ul>
      </div>
    );
  }
}

MainMenu.contextTypes = {
  locale: PropTypes.string
};

export default MainMenu;