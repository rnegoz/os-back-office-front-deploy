import React, { Component, PropTypes } from 'react';
import Button from '../atoms/Button.js';
import Tooltip from './Tooltip';

class FileUploader extends Component {

  handleFileChange(e) {
    let file = this.refs.logoFileInput.files[0];
    let reader = new FileReader();
    let that = this;

    reader.onload = event => {
      let url = event.target.result;
      that.props.handleChange(url);
    };

    reader.readAsDataURL(file);
  }

  handleSubmit() {
    this.refs.logoFileInput.click();
  }

  render() {
    let styles = {
      component: {
        backgroundColor: '#efefef',
        backgroundSize: this.props.url ? 'cover' : '',
        backgroundPosition: 'center center',
        backgroundRepeat: 'no-repeat',
        backgroundImage: this.props.url ? `url(${this.props.url})` : `url(/styles/imgs/icon_picture_placeholder.svg)`,
        position: 'relative'
      },
      tooltip: {
        position: 'absolute', 
        right: '5px',
        top: '5px'
      }
    };

    let tooltip = <div style={styles.tooltip}> 
                    <Tooltip
                      position='bottom'
                      size={24}
                      icon='icon_arrow'
                      sizeIcon={12}
                      backGroundColor='#74b3eb'
                      color='#fff'
                      colorIcon='#fff'
                      width={200}
                      text={this.props.tooltipText}                    
                    />
                  </div>;      

    let accept = 'image/*';

    return (
      <div className='image-uploader' style={styles.component} >
                
        {this.props.hasTooltip && tooltip}

        <input ref='logoFileInput' className='image-uploader__file-input' type='file' name={this.props.name} accept={accept} onChange={this.handleFileChange.bind(this)} />

        <Button 
          type="button" 
          text={this.props.buttonText} 
          classes="btn btn--upload image-uploader__submit"
          handleClick={this.handleSubmit.bind(this)}
          />
      </div>
    );
  }
}

FileUploader.propTypes = {
  url: PropTypes.string,
  type: PropTypes.string,
  buttonText: PropTypes.string,
  name: PropTypes.string,
  hasTooltip: PropTypes.bool,
  tooltipText: PropTypes.string,
  handleChange: PropTypes.func
};

FileUploader.defaultProps = {
  url: '',
  type: 'image',
  buttonText: '',
  name: '',
  hasTooltip: false,
  tooltipText: '',
  handleChange: () => {}
};

export default FileUploader;