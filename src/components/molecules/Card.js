import React from 'react';

// Current default Card has left-side figure and right-side content
// REMEMBER: Card takes the first child as the left-side figure

const Card = (props) => {
  const [firstChild, ...restChildren] = props.children;

  return (
  <div className="card" style={{border: `2px ${props.border} #CCCCCC`}}>
    <div className="card-figure" style={{backgroundColor: `${props.backgroundColor}`}}>
      {firstChild}
    </div>
    <div className="card-body">
      {restChildren}
    </div>
  </div>
  );
};

Card.propTypes = {
  border: React.PropTypes.string.isRequired,
  figure: React.PropTypes.bool,
  children: React.PropTypes.any.isRequired,
  backgroundColor: React.PropTypes.string
};

Card.defaultProps = {
  backgroundColor: '#FFF',
  border: 'dashed'
};

export default Card;
