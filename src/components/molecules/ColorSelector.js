import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';

class ColorSelector extends Component {
 constructor () {
   super();
   this.changeColor = this.changeColor.bind(this);
 }

  changeColor (color) {
    this.props.handleChange(color);
  }

  render () {
    let { colors } = this.props;
    let styles = {
      button: {
        height: this.props.size,
        width:this.props.size
      }
    };

    return (
      <div className="color-selector">
        {colors.map((color, index) => {
          return(
            <div key={index} className={`color-selector-inner ${this.props.mainColor === color && 'pressed'}`}>
              <button
                onClick={this.changeColor.bind(this, color)}
                style={Object.assign({}, styles.button, {backgroundColor: color})} />
            </div>)
        })}
      </div>
    );
  }
}

export default ColorSelector;