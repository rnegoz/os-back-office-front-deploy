import React, { Component, PropTypes } from 'react';
import Icon from '../atoms/Icon'

class Tooltip extends Component {
  constructor() {
    super();
  }
  render () {
  	const { position, size, color, sizeIcon, backGroundColor, width, icon, colorIcon, text } = this.props;
  	var borderColorArrow;
  	switch(position){
  		case "top":
  			borderColorArrow=`${backGroundColor} transparent transparent transparent`;
  			break;
  		case "right":
  			borderColorArrow=`transparent ${backGroundColor} transparent transparent`;
  			break;
  		case "bottom":
  			borderColorArrow=`transparent transparent ${backGroundColor} transparent`;
  			break;
  		case "left":
  			borderColorArrow=`transparent transparent transparent ${backGroundColor}`;
  			break;
  	}
    return (
      <div>
      <div className="tooltip" style={{width: size,height: size, backgroundColor: backGroundColor}} >
        <Icon
          kind={icon}
          size={sizeIcon}
          color={colorIcon}
          classname="tooltipIcon"
        />
        <span className={`tooltiptext ${position}`} style={{backgroundColor: backGroundColor, color: color, width: width}}>
        	<span className="arrow" style={{borderColor: borderColorArrow}} />
        		{text}
        </span>
      </div>
      </div>
    );
  }
}

Tooltip.defaultProps = {
  position: 'right',
  size: 24,
  color: '#fff',
  sizeIcon: 12,
  backGroundColor: '#74b3eb',
  width: 200,
  icon: 'icon_info',
  colorIcon: '#fff',
  text: ''
};

Tooltip.PropTypes = {
  position: PropTypes.string,
  size: PropTypes.number,
  color: PropTypes.string,
  sizeIcon: PropTypes.number,
  backGroundColor: PropTypes.string,
  width: PropTypes.number,
  icon: PropTypes.string,
  colorIcon: PropTypes.string,
  text: PropTypes.string.isRequired
};

export default Tooltip;