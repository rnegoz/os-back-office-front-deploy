import React, { Component, PropTypes } from 'react';
import cx from 'classnames';

class ButtonGroup extends Component {
  constructor(props) {
    super(props);
    this.state = {selectedItem: undefined};
  }

  onLabelSelect = (label, event) => {
    event.preventDefault();
    this.props.handleSelection(label);
  }

  render() {
    return (
      <div className="button-group">
      {React.Children.map(this.props.children, (child, index) => {
        const buttonLabel = this.props.buttonLabels[index];
        const { selectedLabel } = this.props;

        return (
          <button key={index} className={cx('group-btn', {'is-selected': selectedLabel === buttonLabel})} 
            onClick={this.onLabelSelect.bind(null, buttonLabel)}>
            {React.cloneElement(child)}
          </button>
        );
      })
      }
      
      </div>
    );
  }
}

ButtonGroup.propTypes = {
  buttonLabels: PropTypes.array.isRequired,
  selectedLabel: PropTypes.string.isRequired,
  children: PropTypes.array.isRequired,
  handleSelection: PropTypes.func.isRequired
};

export default ButtonGroup;