import React, { Component, PropTypes } from 'react';

import Icon from '../atoms/Icon';

import requireLocalizations from '../../localizations/output/all.js';

class SearchField extends Component {

  constructor(props, context) {
    super(props, context);
    this.l = requireLocalizations(context.locale);
  }

  onSearch = (event = null) => {
    event && event.preventDefault();
    const query = this.refs.searchfield.value;
    this.props.handleSearch(query);
  };

  onKeyPress = (event) => {
    if(event.charCode == 13 || event.keyCode == 13) {
      this.onSearch();
    }
  };

  render() {
    const l = this.l;

    return (
      <div className="search-field">
        <label className="search-field__label">{l('SEARCHFIELD->LABEL')}</label>
        <input onKeyPress={this.onKeyPress} className="search-field__input" ref="searchfield" name="searchfield" type="text" placeholder={l('SEARCHFIELD->PLACEHOLDER')} />
        <Icon handleClick={this.onSearch} color="#CCC" size={20} kind="icon_magnifier" />
      </div>
    );
  }
}

SearchField.contextTypes = {
  locale: PropTypes.string
};

SearchField.propTypes = {
  handleSearch: PropTypes.func.isRequired
};

export default SearchField;
