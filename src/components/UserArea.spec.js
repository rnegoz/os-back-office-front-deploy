import React from 'react';
import sinon from 'sinon';
import {mount, shallow} from 'enzyme';
import {expect} from 'chai';

import requireLocalizations from '../localizations/output/all.js';

import UserArea from './UserArea';

const mountSimple = () => {
  return mount(<UserArea />);
}
const shallowSimple = () => {
  return shallow(<UserArea />);
}

describe('<UserArea />', () => {

  let wrapper;

  it('should call componentDidMount just once', () => {
    sinon.spy(UserArea.prototype, 'componentDidMount');
    wrapper = mountSimple();

    expect(UserArea.prototype.componentDidMount.calledOnce).to.equal(true);
  });

  it('should have own class ', () => {
    wrapper = shallowSimple();
    expect(wrapper.is('.user-area')).to.equal(true);
    expect(wrapper.find('.user-area')).to.be.length(1);
  });

});
