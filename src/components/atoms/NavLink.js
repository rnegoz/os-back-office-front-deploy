import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router'

class NavLink extends Component {
  render() {
    return (
        <Link {...this.props} activeClassName="active">
          {this.props.children}
        </Link>
    );
  }
}

NavLink.propTypes = {
  to: PropTypes.string.isRequired
};

export default NavLink;