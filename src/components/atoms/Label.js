import React, { Component } from 'react';

const typesOfLabels = {
  problematic: '#f57223',
  friendly: '#6ab01d',
  neutral: '#ccc',
  mayor: '#000'
};

export default class Label extends Component {

  static propTypes = {
    color: React.PropTypes.string,
    size: React.PropTypes.number,
    type: React.PropTypes.string,
    classname: React.PropTypes.string
  };

  static defaultProps = {
    size: 16,
    color: '#000',
    classname: 'label'
  }

  render() {
    let { color } = this.props;
    const { size, type, classname } = this.props;

    color = typesOfLabels.hasOwnProperty(type) && typesOfLabels[type];

    return (
      <span className={classname}>
        <svg fill={color} height={size} width={size} viewBox="0 0 16 24" xmlns="http://www.w3.org/2000/svg"><title>icon_label</title><path d="M14.895,0 L0.463,0 C0.226,0 0.033,0.1552 0.033,0.3424 L0.033,23.1448 C0.033,23.2832 0.136,23.4056 0.299,23.46 C0.352,23.4784 0.406,23.4864 0.463,23.4864 C0.575,23.4864 0.683,23.4528 0.764,23.3848 L7.681,17.8576 L14.593,23.3848 C14.714,23.484 14.896,23.5112 15.06,23.46 C15.221,23.4064 15.326,23.2832 15.326,23.1448 L15.326,0.3424 C15.325,0.1552 15.132,0 14.895,0 L14.895,0 Z"/></svg>
      </span>
    );
  }
}