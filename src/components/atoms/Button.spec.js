import React from 'react';
import {shallow} from 'enzyme';
import {expect} from 'chai';

import Button from './Button';

describe('<Button />', () => {
  it('should have own class ', () => {
    const wrapper = shallow(<Button type="" classes="btn btn__primary" />);
    expect(wrapper.is('.btn')).to.equal(true);
    expect(wrapper.find('.btn')).to.be.length(1);
    expect(wrapper.find('.btn__primary')).to.be.length(1);
  });

  it('should display button text ', () => {
    const text = 'Test';
    const wrapper = shallow(<Button text={text} type="" classes="" />);
    expect(wrapper.find('.btn__text').text()).to.equal(text);
  });
});
