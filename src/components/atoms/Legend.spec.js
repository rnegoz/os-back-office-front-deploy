import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import Legend from './Legend.js';

describe('<Legend />', () => {
  const wrapper = shallow(<Legend color="black" text="Test" />);

  it('should have props for color', () => {
    expect(wrapper.props().color).to.be.defined;
  });

  it('should have props for text', () => {
    expect(wrapper.props().text).to.be.defined;
  });

});
