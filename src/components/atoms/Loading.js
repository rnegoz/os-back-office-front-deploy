import React, { Component, PropTypes } from 'react';

class Loading extends Component {
  render() {
    const size = this.props.size ? this.props.size : '1rem';
    let divStyle = {
      width: size,
      height: size
    };
    return (
        <div className="loading downhill" style={divStyle}></div>
    );
  }
}

Loading.propTypes = {
  size: PropTypes.string
};

export default Loading;