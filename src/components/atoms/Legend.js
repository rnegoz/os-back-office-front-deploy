import React from 'react';

const Legend = (props) => {
  return (
    <div className="legend">
      <div className="legend-ref" style={{ backgroundColor: `${props.color}` }}></div>
      <span className="legend-text">{props.text}</span>
    </div>
  );
};

Legend.propTypes = {
  color: React.PropTypes.string.isRequired,
  text: React.PropTypes.string.isRequired
};

export default Legend;
