import React, { Component, PropTypes } from 'react';
import Icon from './Icon';
import classnames from 'classnames';

const displayIcons = {
  isLoading: 'icon_loading',
  isError: 'icon_error'
};

class Button extends Component {

  renderIcon() {
    const {iconColor, display} = this.props;

    let displayIcon = this.props.icon;
    if(display === 'isLoading')
      displayIcon = displayIcons.isLoading;
    if(display === 'isError')
      displayIcon = displayIcons.isError;

    return (<Icon kind={displayIcon} size={24} color={iconColor} />);
  }

  render() {
    const {type, children, icon, text, display} = this.props;
    let {classes} = this.props;

    if(display === 'isLoading')
      classes = `${classes} btn--is-loading`;
    if(display === 'isError')
      classes = `${classes} btn--is-error`;

    return (
      <button onClick={this.props.handleClick}
        type={type}
        className={classes}>
        {children || <span className="btn__text">{text}</span>}
        {icon && this.renderIcon()}
      </button>
    );
  }
}

Button.defaultProps = {
  type: 'submit',
  text: '',
  classes: 'btn',
  icon: '',
  iconColor: '#FFF',
  display: ''
};

Button.propTypes = {
  type: PropTypes.string,
  text: PropTypes.string.isRequired,
  classes: PropTypes.string,
  icon: PropTypes.string,
  iconColor: PropTypes.string,
  display: PropTypes.string,
  handleClick: PropTypes.func
};

export default Button;
