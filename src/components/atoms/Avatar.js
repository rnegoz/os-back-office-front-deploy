import React, { Component, PropTypes } from 'react';

class Avatar extends Component {
  render() {
    return (
      <img className="avatar" src={this.props.src} />
    );
  }
}

Avatar.propTypes = {
  src: PropTypes.string.isRequired
};

export default Avatar;