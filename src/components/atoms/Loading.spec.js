import React from 'react';
import {shallow } from 'enzyme';
import {expect} from 'chai';

import Loading from './Loading';

describe('<Loading />', () => {
  it('should have own class ', () => {
    const wrapper = shallow(<Loading />);
    expect(wrapper.is('.loading')).to.equal(true);
    expect(wrapper.find('.loading')).to.be.length(1);
  });
});
