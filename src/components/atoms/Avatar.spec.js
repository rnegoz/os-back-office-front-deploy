import React from 'react';
import {shallow} from 'enzyme';
import {expect} from 'chai';

import Avatar from './Avatar';

describe('<Avatar />', () => {
  it('should have own class ', () => {
    const wrapper = shallow(<Avatar src="" />);
    expect(wrapper.is('.avatar')).to.equal(true);
    expect(wrapper.find('.avatar')).to.be.length(1);
  });

  it('should have an provided image source ', () => {
    const imgSrc = 'https://randomuser.me/api/portraits/med/men/49.jpg';
    const wrapper = shallow(<Avatar src={imgSrc} />);
    expect(wrapper.find('img')).to.have.length(1);
    expect(wrapper.html()).to.equal(
      `<img class="avatar" src="https://randomuser.me/api/portraits/med/men/49.jpg"/>`
    );
  });
});
