import React, { Component, PropTypes } from 'react';

import requireLocalizations from '../localizations/output/all.js';
import Avatar from './atoms/Avatar';
import Icon from './atoms/Icon';

class UserArea extends Component {

  constructor(props, context) {
    super(props, context);
    this.l = requireLocalizations(context.locale);
  }

  componentDidMount() {}

  render() {
    return (
      <div className="user-area">
        <Avatar src="https://randomuser.me/api/portraits/med/men/49.jpg" />
        <h4 className="user-area__name">Username</h4>
        <span className="user-area__messages missing">
          <Icon kind="icon_mail" size={16} color="#000" />
        </span>
        <span className="user-area__alerts missing">
          <Icon kind="icon_notifications" size={16} color="#000" />
        </span>
      </div>
    );
  }
}

UserArea.contextTypes = {
  locale: PropTypes.string
};

export default UserArea;
