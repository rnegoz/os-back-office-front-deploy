import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import * as actionCreators from '../reducers/permissionsReducer';

class PermissionsPage extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
      <h1>Permissions</h1>
        <img src="/imgs/account_mock.png" className="placeholder__mock-bg"></img>
      </div>
    );
  }
}

PermissionsPage.PropTypes = {
  actions: PropTypes.object.isRequired,
  permissions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {
    permissions: state.permissions
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actionCreators, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PermissionsPage);
