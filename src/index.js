/* eslint-disable */
import React from 'react';
import {render} from 'react-dom';
import { Router, browserHistory } from 'react-router';

import jwtDecode from 'jwt-decode';

import { Provider } from 'react-redux';
import configureStore from './store/configureStore';
import { syncHistoryWithStore } from 'react-router-redux';

// import { loginSuccess } from './actions/loginActions';
// import setAuthToken from './utils/setAuthToken';

import routes from './routes';
import './styles/styles.scss';

const store = configureStore();
const history = syncHistoryWithStore(browserHistory, store);

// if (localStorage.jwtToken) {
//   setAuthToken(localStorage.jwtToken);
//   store.dispatch(loginSuccess(jwtDecode(localStorage.jwtToken)));
// }

render(
  <Provider store={store}>
    <Router history={history} routes={routes} />
  </Provider>, document.getElementById('app')
);
/* eslint-enable */
