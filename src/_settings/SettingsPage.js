import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import { Link } from 'react-router';
import {Container, Row, Col, Hidden, Visible} from 'react-grid-system';

import {
  changeLogo,
  changeBackground,
  changeMessage,
  changeMainColor
} from '../reducers/settingsReducer';
import Button from '../components/atoms/Button';
import SkyLight from 'react-skylight';
import ModalStyles from '../utils/ModalStyles';
import FileUploader from '../components/molecules/FileUploader';
import ColorSelector from '../components/molecules/ColorSelector';

//import * as actionCreators from '../reducers/userDirectoryReducer';
import requireLocalizations from '../localizations/output/all.js';

class SettingsPage extends Component {

  constructor(props, context) {
    super(props, context);
    //this.l = requireLocalizations(context.locale); TODO
  }

  componentDidMount() {
    //this.props.actions.fetchTenantSettings();
  }

  handleChangeMessage(ev) {
    this.props.actions.changeMessage(ev.target.value);
  }


  render() {
    // const l = this.l;
    const styles = {
      flexContainer: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',        
        alignContent: 'flex-start',
        alignItems: 'stretch'
      },
      flexCol1: {
        width: '200px',
        marginBottom: '15px',
        marginRight: '10px'
      },
      flexCol3: {
        flexGrow: 3
      },
      'inputMessage': {
        margin: '10px 0px 20px',
        width: '100%'
      },
      'colorBox': {
        width: '100px',
        height: '100px',
        borderRadius: '8px',
        border: '1px solid #ccc',
        marginRight: '10px'
      },
      'lastModifiedText': {
        float: 'right',
        fontSize: '12px',
        color: '#aaa'
      }

    };

    return (
      <section className="tmpl-settingsdirectory {classname}">
        <Hidden xs sm> 
          <p style={styles.lastModifiedText}>Last Modified by `Donald Quinn` 1 hour ago</p> 
        </Hidden>
        <h1>Manage your settings</h1>
        
        <Hidden xs sm> <br /> </Hidden>

        <h2>Build your cover:</h2>
        <div style={styles.flexContainer}>
          <div style={styles.flexCol1}>
            <FileUploader
              name='logo'
              handleChange={this.props.actions.changeLogo}
              buttonText='UPLOAD YOUR LOGO'
              url={this.props.logo}
              hasTooltip={true}
              tooltipText='Upload files in .jpg, .png or .gif format. Recommended size: Squared image, 180px for the highest value.'
            />                 
          </div>

          <div style={styles.flexCol3}>
            <FileUploader
              name='background'
              handleChange={this.props.actions.changeBackground}
              buttonText='UPLOAD YOUR BACKGROUND IMAGE'
              url={this.props.background}
              hasTooltip={true}
              tooltipText='Upload files in .jpg, .png or .gif format. Minimal width: 800px.Recommended size: 180px height for the highest value.'
            />
          </div>
        </div>

        <div className='settings settings--background'></div>

        <br />
        <h2>Write your main message:</h2>        
        <input 
          type='text' 
          placeholder='Write here a description or main message for your city (no more than 150 characters)' 
          style={styles.inputMessage} 
          onChange={this.handleChangeMessage.bind(this)} 
          value={this.props.mainMessage} />

        <br /><br />
        <h2>Change your main colour:</h2>
        <div style={styles.flexContainer} style={{ marginTop: '0px'}}>
          <ColorSelector 
            mainColor={this.props.mainColor}
            handleChange={this.props.actions.changeMainColor}
            size={35}
            colors={['#ff0000','#4a4a4a','#7ac229','#71b2ed','#f57223','#bd10e0']}
          />
        </div>

        <br /><br />
        <Link to={`/admin/settings/preview`}>
          <Button 
            type="submit" 
            text="PREVIEW" 
            classes="btn btn--icon-right btn--is-primary" 
            icon="icon_view"            
            />
        </Link>

        <Button 
          type="submit" 
          text="SAVE" 
          classes="btn btn--icon-right btn--is-secondary btn--green" 
          icon="icon_check" 
          />
      </section>
    );
  }
}


function mapStateToProps(state) {
  return state.settings;
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({
      changeLogo, 
      changeBackground, 
      changeMessage,
      changeMainColor
    },
    dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SettingsPage);