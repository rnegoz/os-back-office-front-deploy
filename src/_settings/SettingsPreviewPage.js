import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Container, Row, Col, Visible, Hidden} from 'react-grid-system';
import { Link } from 'react-router';

import Icon from '../components/atoms/Icon';
import requireLocalizations from '../localizations/output/all.js';

class SettingsPreviewPage extends Component {

  constructor(props, context) {
    super(props, context);
  }

  render() {
    let backgroundHeight = '150px';
    let mainColor = this.props.mainMessage;
    const styles = {
      container: {
        position: 'relative',
        height: '150px'
      },
      header: {
        lineHeight: '3.5em',
        borderBottom: '1px solid #eee'
      },
      background: {
        width: '100%',
        height: backgroundHeight,
        background: `#aaa url(${this.props.background}) no-repeat top center`,
        backgroundSize: 'cover',
        position: 'absolute',
        top: '0',
        left: '0'
      },
      logo: {
        background: `#ddd url(${this.props.logo}) no-repeat top center`,
        backgroundSize: 'cover',
        maxWidth: '100%',
        minWidth: '150px',
        minHeight: '150px',
        height: '0',
        paddingBottom: '100%',
        verticalAlign: 'middle'
      },
      logoXS: {
        background: `#ddd url(${this.props.logo}) no-repeat top center`,
        backgroundSize: 'cover',
        maxWidth: '38%',
        height: '0',
        minWidth: '150px',
        minHeight: '150px',
        paddingBottom: '38%',
        verticalAlign: 'middle',
        marginTop: '5px'
      },
      searcher: {
        verticalAlign: 'middle',
        lineHeight: '1.2em',
        padding: '.5em 1em',
        display: 'inline-block',
        fontSize: '.8em',
        backgroundColor: `${this.props.mainColor}`,
        color: '#fff',
        borderRadius: '.3em',
        marginRight: '8px'
      },
      mainTop:{
        marginTop: backgroundHeight
      },
      main: {
        paddingTop: '2em',
        lineHeight: '1.5'
      },
      mainRight: {
        marginTop: backgroundHeight,
        lineHeight: '1.5'
      },
      registerButton: {
        backgroundColor: 'green',
        fontSize: '1em',
        color: '#fff',
        padding: '.5em 1em',
        border: '0px',
        borderRadius: '.3em',
        display: 'block',
        fontSize: '.8em',
        fontWeight: 'bold',
        margin: '1em auto 0'
      },
      message: {
        verticalAlign: 'middle',
        lineHeight: '1.2em',
        padding: '1em',
        fontSize: '1.4em',
        color: '#555',
        borderRadius: '.3em',
        textAlign: 'center',
        lineHeight: '1.5'
      },
      headerButton: {
        border: `1px solid ${this.props.mainColor}`,
        marginRight: '5px',
        heigth: '30px',
        verticalAlign: 'middle',
        lineHeight: '1.2em',
        padding: '.5em 1em',
        display: 'inline-block',
        fontSize: '.8em',
        borderRadius: '.3em',
        color: `${this.props.mainColor}`
      },
      menuLeft: {
        borderLeft: '1px solid #eee',
        marginTop: '20px',
        paddingTop: '20px',
        paddingBottom: '10px'
      },
      menuRight: {
        marginTop: backgroundHeight,
        paddingTop: '20px',
        paddingBottom: '10px'
      },
      menuItem: {
        border: `1px solid ${this.props.mainColor}`,
        borderRadius: '5px',
        backgroundColor: `${this.props.mainColor}`,
        color: `${this.props.mainColor}`,
        marginBottom: '25px',
        lineHeight: '18px',
        marginLeft: '10px',
        width: 'auto',
        display: 'inline-block'
      },
      tagItem: {
        border: `1px solid ${this.props.mainColor}`,
        borderRadius: '5px',
        color: `${this.props.mainColor}`,
        marginBottom: '15px',
        lineHeight: '28px',
        width: 'auto',
        display: 'inline-block',
        color: '#fff',
        marginLeft: '10px'
      },
      item: {
        height: '150px',
        backgroundColor: '#eee',
        marginTop: '20px',
        borderRadius: '8px'
      },
      closeBarLeft:{
        height: '40px',
        textAlign: 'left',
        fontSize: '18px',
        marginBottom: '10px',
        padding: '10px',
        backgroundColor: '#000',
        color: '#fff',
        boxShadow: '0px 6px 5px 0px rgba(0,0,0,0.4)'
      },
      closeBarRight:{
        height: '40px',
        textAlign: 'right',
        fontSize: '18px',
        marginBottom: '10px',
        padding: '10px',
        backgroundColor: '#000',
        color: '#fff',        
        boxShadow: '0px 6px 5px 0px rgba(0,0,0,0.4)'
      },
      closeIcon:{
        padding: '15px',
        cursor: 'pointer',
        position: 'relative',
        top: '0px',
        right: '10px',
        verticalAlign: 'middle',
        lineHeight: '5px'
      },
      backIcon:{
      },
      closeTxt:{
        verticalAlign: 'top',
        marginLeft: '10px',
        marginTop: '5px',
        display: 'inline-block',
        color: '#fff'
      }
    }
;
    return (
      <div>
        <div>
          <Link to={`/admin/settings`} style={styles.closeIcon}> 
            <Col xs={12} sm={6} style={styles.closeBarLeft}>            
              <Icon
                kind='icon_arrow_left'
                size={16}
                color='#fff'
                className='rotate(90deg)'
              />
              <p style={styles.closeTxt}> Go Back </p>
            </Col>
            <Hidden xs>
              <Col xs={6} sm={6} style={styles.closeBarRight}>            
                  <p style={styles.closeTxt}> This is only a preview </p>              
              </Col>
            </Hidden>
          </Link>  
        </div>  
        <div className='previewPage'>
          <Container>
            <Row style={styles.header}>
              <Col xs={1} sm={2}>
                <Visible xs>
                  <Icon
                    kind='icon_adress'
                    size={18}
                    color={this.props.mainColor}
                  />
                </Visible>
                <Hidden xs>
                  <img src='/imgs/cvct-logo-principal.png' style={{maxWidth:'120px', width: '100%', display: 'inline-block', verticalAlign: 'middle'}} />
                </Hidden>
              </Col>
              <Col xs={11} sm={6} md={7}>
                <div style={styles.searcher}>
                  LAS PALMAS DE GRAN CANARIA
                </div>
                <Icon
                  kind='icon_magnifier'
                  size={18}
                  color={this.props.mainColor}
                />
              </Col>
              <Hidden xs>
                <Col sm={4} md={3}>
                  <p style={styles.headerButton}>Registrarse</p>
                  <p style={styles.headerButton}>Sign in</p>
                  <p style={styles.headerButton}>es</p>
                </Col>
              </Hidden>  
            </Row>
          </Container>
          <div style={styles.container}>
            <div style={styles.background}></div>
            <Container>
              <Row>
                <Col sm={2}>
                  <Visible xs>
                    <div style={styles.logoXS}></div>
                  </Visible>

                  <Visible sm md lg>
                    <div style={styles.logo}></div>
                  </Visible>

                  <Hidden xs>
                    <div style={styles.menuLeft}>
                      <div style={styles.menuItem}> ============ </div>
                      <div style={styles.menuItem}> ======== </div>
                      <div style={styles.menuItem}> =========== </div>
                      <div style={styles.menuItem}> ====== </div>
                      <div style={styles.menuItem}> ============= </div>
                    </div>
                  </Hidden>
                </Col>

                <Col sm={8} style={styles.main}>
                  <Hidden xs>
                    <div style={styles.mainTop}></div>
                  </Hidden>

                  <div style={styles.message}>
                    {this.props.mainMessage}

                    <div style={styles.item}></div>
                    <div style={styles.item}></div>
                    <div style={styles.item}></div>
                  </div>
                </Col>

                <Col xs={12} sm={2} style={styles.menuRight}>
                  <div>
                    <div style={styles.menuItem}> ============ </div>
                    <div style={styles.tagItem}> ============== </div>
                    <div style={styles.tagItem}> =========== </div>
                    <div style={styles.tagItem}> ========== </div>
                    <div style={styles.tagItem}> =============== </div>
                    <div style={styles.tagItem}> === </div>
                    <div style={styles.tagItem}> ======= </div>
                    <div style={styles.tagItem}> ========== </div>
                    <div style={styles.tagItem}> ============= </div>
                    <div style={styles.tagItem}> ============= </div>
                    <div style={styles.tagItem}> ======= </div>
                    <div style={styles.tagItem}> =========== </div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </div>
      </div>
    );
  }
}


function mapStateToProps(state) {
  return state.settings;
}

function mapDispatchToProps(dispatch) {
  return {};
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SettingsPreviewPage);