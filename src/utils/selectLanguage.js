export default function selectLanguage() {

  // TECH-DEBT: get language from cookie / browser / default
  let lang = navigator.languages
        ? navigator.languages[0]
        : (navigator.language || navigator.userLanguage);

  return lang;
}
