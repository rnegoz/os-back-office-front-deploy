
const styles = {
  overlayStyles: {
    position: 'fixed',
    top: '0px',
    left: '0px',
    width: '100%',
    height: '100%',
    zIndex: '99',
    backgroundColor: 'rgba(0,0,0,0.3)',
  },
  dialogStyles: {
    width: '700px',
    height: '400px',
    position: 'fixed',
    top: '50%',
    left: '50%',
    margin: '0 auto',
    transform: 'translate(-50%,-50%)',
    // marginTop: 'null',
    // marginLeft: 'auto',
    backgroundColor: '#fff',
    borderRadius: '5px',
    zIndex: '100',
    padding: '20px',
    boxShadow: '0px 0px 5px rgba(0,0,0,.14),0px 4px 8px rgba(0,0,0,.28)',
  },
  title: {
    marginTop: '0px',
  },
  closeButtonStyle: {
    cursor: 'pointer',
    position: 'absolute',
    fontSize: '1.8em',
    right: '10px',
    top: '10px',
  },
};

export default styles;