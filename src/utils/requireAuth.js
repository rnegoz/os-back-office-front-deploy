import React from 'react';
import { connect } from 'react-redux';

export default function(ComposedComponent) {

  class Authenticate extends React.Component {

    componentWillMount() {
      if(!this.props.isAuthenticated) {
        // redirect to login page
        this.context.router.push('/login');
      }
    }

    componentWillUpdate(nextProps) {
      if(!nextProps.isAuthenticated) {
        // redirect to home page
        this.context.router.push('/');
      }
    }

    render() {
      return (<ComposedComponent {...this.props} />);
    }
  }

  Authenticate.propTypes = {
    isAuthenticated: React.PropTypes.bool.isRequired
  };

  Authenticate.contextTypes = {
    router: React.PropTypes.object.isRequired
  };

  function mapStateToProps(state) {
    return {
      isAuthenticated: state.auth.isAuthenticated
    };
  }

  return connect(mapStateToProps)(Authenticate);

}
