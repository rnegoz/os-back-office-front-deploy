import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import * as actionCreators from '../reducers/accountReducer';

class SentimentAPage extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
      <h1>Sentiment Analysis</h1>
        <img src="/imgs/sentiments_mock.png" className="placeholder__mock-bg" style={{ height:"100%" }}></img>
      </div>
    );
  }
}

SentimentAPage.PropTypes = {
  actions: PropTypes.object.isRequired,
  analysis: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {
    analysis: state.analysis
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actionCreators, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SentimentAPage);
