import React, { Component, PropTypes } from 'react';

import requireLocalizations from '../localizations/output/all.js';

class WelcomePage extends Component {

  constructor(props, context) {
    super(props, context);
    this.l = requireLocalizations(context.locale);
  }

  render() {
    const cityname = "Test City";
    const l = this.l;
    return (
      <div>
        <h1>{l('WELCOME_PAGE->WELCOME', {cityname})}</h1>
      </div>
    );
  }
}

WelcomePage.contextTypes = {
  locale: PropTypes.string
};

export default WelcomePage;
