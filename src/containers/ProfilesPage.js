import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import * as actionCreators from '../reducers/profilesReducer';

class ProfilesPage extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <img src="/imgs/business_profiles_mock.png" className="placeholder__mock-bg"></img>
      </div>
    );
  }
}

ProfilesPage.PropTypes = {
  actions: PropTypes.object.isRequired,
  profiles: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {
    profiles: state.profiles
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actionCreators, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProfilesPage);
