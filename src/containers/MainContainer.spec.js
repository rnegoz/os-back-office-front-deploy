import React from 'react';
import sinon from 'sinon';
import {mount} from 'enzyme';
import {expect} from 'chai';

import requireLocalizations from '../localizations/output/all.js';

import MainContainer from './MainContainer';

const mountSimple = () => {
  return mount(<MainContainer />);
}

describe('<MainContainer />', () => {

  let wrapper;

  it('should call componentDidMount just once', () => {
    sinon.spy(MainContainer.prototype, 'componentDidMount');
    wrapper = mountSimple();

    expect(MainContainer.prototype.componentDidMount.calledOnce).to.equal(true);
  });

});
