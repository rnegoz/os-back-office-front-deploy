import React from 'react';
import sinon from 'sinon';
import {mount} from 'enzyme';
import {expect} from 'chai';

import { Provider } from 'react-redux';
import configureStore from '../store/configureStore';

import requireLocalizations from '../localizations/output/all.js';

import WelcomePage from './WelcomePage';

// const mountSimple = (store) => {
//   return mount(
//       <Provider store={store}>
//         <WelcomePage />
//       </Provider>);
// }

// describe('<WelcomePage />', () => {

//   let store;
//   let wrapper;

//   beforeEach(() => {
//     store = configureStore();
//   });

//   it('should call componentDidMount just once', () => {
//     sinon.spy(WelcomePage.prototype, 'componentDidMount');
//     wrapper = mountSimple(store);

//     expect(WelcomePage.prototype.componentDidMount.calledOnce).to.equal(true);
//   });

// });
