import React from 'react';
import sinon from 'sinon';
import axios from 'axios';
import moxios from 'moxios';
import { expect } from 'chai';
import { mount } from 'enzyme';

import { Provider } from 'react-redux';
import configureStore from '../store/configureStore';

import requireLocalizations from '../localizations/output/all.js';
import Login from './Login';

const mountSimple = (store) => {
  return mount(
      <Provider store={store}>
        <Login />
      </Provider>);
}

describe('<Login />', () => {

  const validUsername = 'luke_skywalker@laresistance.com';
  const validPassword = 'WhosUrDaddy@619';
  const credentials = { username: validUsername, password: validPassword };

  let store;
  let wrapper;

  beforeEach(() => {
    store = configureStore();
    moxios.install();
  });

  afterEach(() => {
    moxios.uninstall();
  });

  it('should call componentDidMount just once', () => {
    // sinon.spy(Login.prototype, 'componentDidMount');
    // wrapper = mountSimple(store);

    // expect(Login.prototype.componentDidMount.calledOnce).to.equal(true);
  });

  it('should have an input for username', () => {
    // wrapper = mountSimple(store);
    // const usernameInput = wrapper.find('input[name="username"]');

    // expect(usernameInput.length).to.equal(1);
  });

  it('should have an input for password', () => {
    // wrapper = mountSimple(store);
    // const passwordInput = wrapper.find('input[name="password"]');

    // expect(passwordInput.length).to.equal(1);
  });

  describe('when form validation fails', () => {

    it('should display an error on submit', () => {
      const l = requireLocalizations('en-US');
      const wrapper = mount(
        <Provider store={store}>
          <Login />
        </Provider>
      ,{
        context: {locale: 'en-US'},
        contextTypes: {locale: React.PropTypes.string}
      });

      const loginForm = wrapper.find('form');
      const errors = wrapper.find('.error');

      expect(errors.at(0).text()).to.equal('');
      expect(errors.at(1).text()).to.equal('');

      loginForm.simulate('submit');

      // expect(errors.at(0).text()).to.equal('An email is required.');
      // expect(errors.at(1).text()).to.equal('A password is required.');
    });

  });

  describe('when form validation passes', () => {

    // beforeEach(() => {
    //   wrapper = mount(
    //     <Provider store={store}>
    //       <Login />
    //     </Provider>);
    //   wrapper
    //     .find('input[name="username"]')
    //     .simulate('change', {target: {value: validUsername}});
    //   wrapper
    //     .find('input[name="password"]')
    //     .simulate('change', {target: {value: validPassword}});
    // });

    // it('should display an error message when login fails', () => {
    //   const loginForm = wrapper.find('form');
    //   const formError = wrapper.find('.form-error');

    //   moxios.stubRequest(API.LOGIN, {
    //     status: 401,
    //     response: { message: 'Access denied.'}
    //   });

    //   loginForm.simulate('submit');

    //   moxios.wait(() => {
    //     expect(formError.at(0).text()).to.equal('Acces denied.');
    //     done();
    //   });
    // });

  });

});
