import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import Card from '../components/molecules/Card.js';
import Button from '../components/atoms/Button.js';
import Icon from '../components/atoms/Icon.js';
import Legend from '../components/atoms/Legend.js';

import * as actionCreators from '../reducers/consultationsReducer';

class ConsultationPage extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <h1>Consultations</h1>

        <div className="legend-container__horizontal">
          <Legend color="orange" text="DRAFT"/>
          <Legend color="green" text="OPEN"/>
          <Legend color="black" text="CLOSED"/>
        </div>

        <Card border="dotted" figure backgroundColor="orange">
          <Icon kind="icon_loading" color="#FFF" />
          <div className=".card-text__container">
            <h3>Some title here</h3>
            <p>Some text here</p>
          </div>
          <Button classes="btn btn--is-white btn--icon-left" icon="icon_view" >
            <a href="http://52.50.155.158/?filename=montserrat" target="_blank">
              VIEW
            </a>
          </Button>
        </Card>

        <Card border="dotted" figure backgroundColor="green">
          <Icon kind="icon_arrow" color="#FFF" />
          <div className=".card-text__container">
            <h3>Some title here</h3>
            <p>Some text here</p>
          </div>
          <Button classes="btn btn--is-white btn--icon-left" icon="icon_view" >
            <a href="http://52.50.155.158/?filename=tally_es" target="_blank">
              VIEW
            </a>
          </Button>
        </Card>

        <Card border="dotted" figure backgroundColor="green">
          <Icon kind="icon_arrow" color="#FFF" />
          <div className=".card-text__container">
            <h3>Some title here</h3>
            <p>Some text here</p>
          </div>
          <Button classes="btn btn--is-white btn--icon-left" icon="icon_view" >
            <a href="http://52.50.155.158/?filename=tally_1" target="_blank">
              VIEW
            </a>
          </Button>
        </Card>

      </div>
    );
  }
}

ConsultationPage.PropTypes = {
  actions: PropTypes.object.isRequired,
  consultations: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {
    consultations: state.consultations
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actionCreators, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ConsultationPage);
