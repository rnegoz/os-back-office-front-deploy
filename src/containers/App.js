import React, { Component, PropTypes } from 'react';

// TECH-DEBT : load/write cookies
import selectLanguage from '../utils/selectLanguage';
import {LANGUAGES} from '../constants/constants';

class App extends Component {

  getChildContext() {

    let locale = selectLanguage();
    if(LANGUAGES.indexOf(locale) === -1) {
      locale = LANGUAGES[0];
    }
    return {
      locale
    };
  }

  render() {
    return (
      <div className="app-wrapper">
        {this.props.children}
      </div>
    );
  }
}

App.propTypes = {
  children: PropTypes.element
};

App.childContextTypes = {
  locale: PropTypes.string
};

export default App;