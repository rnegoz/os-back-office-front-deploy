import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import * as actionCreators from '../reducers/accountReducer';

class ValidationPage extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <img src="/imgs/validation_mock.png" className="placeholder__mock-bg"></img>
      </div>
    );
  }
}

ValidationPage.PropTypes = {
  actions: PropTypes.object.isRequired,
  users: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {
    users: state.users
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actionCreators, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ValidationPage);
