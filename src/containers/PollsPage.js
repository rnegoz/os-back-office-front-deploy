import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import SearchField from '../components/molecules/SearchField';

import PollsCards from '../recharts/PollsCards';

import * as actionCreators from '../reducers/pollsReducer';

const data =[{id:1,voting_enabled:true,poll_name:'test1',creation_date:'21/01/12',total_votes:12},
            {id:2,voting_enabled:false,poll_name:'test2',creation_date:'21/01/11',total_votes:1},
            {id:3,voting_enabled:false,poll_name:'test3',creation_date:'21/01/13',total_votes:31}];

class PollsPage extends Component {
  constructor(props) {
    super(props);
  }

  delegatedSearch = (query) => {
    console.log(query);
  };

  render() {
    return (
      <section className="tmpl-advpolls">
        <h1>Advanced Polls</h1>
        <SearchField handleSearch={this.delegatedSearch} />
        
        {data.map(data => {
          return (<PollsCards
                  key={data.id}
                  data={data}
                  />);
        })}
      </section>
    );
  }
}

PollsPage.PropTypes = {
  actions: PropTypes.object.isRequired,
  polls: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {
    polls: state.polls
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actionCreators, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PollsPage);
