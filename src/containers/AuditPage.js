import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import * as actionCreators from '../reducers/auditReducer';

class AuditPage extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <h1>Moderation Audit</h1>
      </div>
    );
  }
}

AuditPage.PropTypes = {
  actions: PropTypes.object.isRequired,
  users: PropTypes.object.isRequired,
  content: PropTypes.object.isRequired,
  profiles: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {
    users: state.users,
    content: state.content,
    profiles: state.profiles
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actionCreators, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AuditPage);
