import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import * as actionCreators from '../reducers/moderationReducer';

class ModerationPage extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <img src="/imgs/content_moderate_mock.png" className="placeholder__mock-bg"></img>
      </div>
    );
  }
}

ModerationPage.PropTypes = {
  actions: PropTypes.object.isRequired,
  content: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {
    content: state.content
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actionCreators, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ModerationPage);
