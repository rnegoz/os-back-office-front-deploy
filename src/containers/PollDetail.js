import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import * as actionCreators from '../reducers/pollsReducer';

import Loading from '../components/atoms/Loading.js';
import CardChart from '../recharts/CardChart.js';
import BarChartComponent from '../recharts/BarChartComponent.js';
import LineChartComponent from '../recharts/LineChartComponent.js';
import DonutChartComponent from '../recharts/DonutChartComponent.js';
import AreaChartComponent from '../recharts/AreaChartComponent.js';

class PollDetail extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
      <h1>Poll Detail</h1>
        <section className="tmpl-polldetail">

          <CardChart title="BarChart - Total votes by hour" class="rechart" border="solid" loading={true}>
            <BarChartComponent  data={dataHours} layoutChart="vertical"
             margin={{top: 25, right: 40, left: 10, bottom: 5}}
             typeX="number" typeY="category" yDataKey="name"
             cols={['votes']}
             colors={['#7AC0FF', '#F68D4D', '#71B2ED', '#9B9B9B']} 
             field={['Total votes']}
             optionsQuestion={false}
             CartesianGridHide={true}
             XAxisHide={true}
             width={200} height={270}
            />
          </CardChart>

          <CardChart title="BarChart - RangeAge(Options)" class="rechart" border="solid" loading={true}>
            <BarChartComponent xDataKey="name" data={dataAge}
             /*width={500} height={270}*/
             margin={{top: 45, right: 80, left: 0, bottom: 5}}
             labelX="Options" labelY="Number of votes"
             cols={["less_18","between_18_24", "more_25_34"]} 
             colors={['#7AC0FF', '#4A4A4A', '#9BD757', '#9B9B9B']}
             field={["-18","18 - 24","25 - 34"]}
            />
          </CardChart>

          <CardChart title="DonutChart" class="rechart" loading={true} border="solid">
            <DonutChartComponent data={dataDonut}/>
          </CardChart>

          <CardChart title="LineChart" class="rechart" loading={true} border="solid">
            <LineChartComponent  data={[{"female":47,"male":62,"year":2016,"month":9,"day":1,"day_votes":1},{"female":21,"male":47,"year":2016,"month":9,"day":2,"day_votes":1},{"female":19,"male":17,"year":2016,"month":9,"day":3,"day_votes":1}]}
            margin={{top: 45, right: 60, left: 0, bottom: 5}}
            labelX="Days" xDataKey="day" labelY="Number of votes"
            cols={["female","male"]} 
            colors={['#7AC0FF', '#4A4A4A', '#71B2ED', '#9B9B9B']} 
            field={['Female','Male']}
            />
          </CardChart>

          <CardChart title="LineChart" class="rechart" loading={true} border="solid">
            <AreaChartComponent xDataKey="name" data={dataAge}
             margin={{top: 45, right: 80, left: 0, bottom: 5}}
             labelX="Options" labelY="Number of votes"
             cols={["less_18","between_18_24", "more_25_34"]}
             field={["-18","18 - 24","25 - 34"]}
             colors={['#7AC0FF', '#4A4A4A', '#9BD757', '#9B9B9B']}
             stackId="a"
            />
          </CardChart>

        </section>
      </div>
    );
  }
}

PollDetail.PropTypes = {
  actions: PropTypes.object.isRequired,
  polls: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {
    polls: state.polls
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actionCreators, dispatch)
  };
}

const dataHours = [{"name":"0-4","votes":15},{"name":"4-8","votes":18},{"name":"8-12","votes":37},{"name":"12-16","votes":50},{"name":"16-20","votes":70},{"name":"20-24","votes":23}];
const dataAge = [{between_18_24:35,less_18:12,more_25_34:39,name:"A"},{between_18_24:45,less_18:7,more_25_34:75,name:"B"}];
const dataDonut = [{"name":"Female","value":10},{"name":"Male","value":20}];
const dataResult = [{"name":"A","value":86},{"name":"B","value":127}];

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PollDetail);
