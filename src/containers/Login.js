import React, { Component, PropTypes } from 'react';
import { reduxForm, Field } from 'redux-form';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import validator from 'validator';

import LoginFormInput from '../components/LoginFormInput';
import Button from '../components/atoms/Button';

import * as actionCreators from '../reducers/loginReducer';
import requireLocalizations from '../localizations/output/all.js';

const validate = (values) => {
  const errors = {},
        username = values.username || '',
        password = values.password || '';

  errors.username = (!validator.isEmail(username)) ? 'LOGIN_PAGE->ERROR-INVALID-EMAIL' : null;
  errors.username = (!username) ? 'LOGIN_PAGE->ERROR-REQUIRED-EMAIL' : null;
  errors.password = (!password) ? 'LOGIN_PAGE->ERROR-REQUIRED-PASSWORD' : null;

  return errors;
};

class Login extends Component {

  constructor(props, context) {
    super(props, context);
    this.l = requireLocalizations(context.locale);
  }

  render() {
    const { submitting, formError, handleSubmit, loginRequest } = this.props;
    const l = this.l;

    // submit button states
    let buttonDisplay = '';
    if(submitting) {
      buttonDisplay = 'isLoading';
    }
    if (formError != undefined && formError != '') {
      buttonDisplay = 'isError';
    }

    return (
      <form className="login-form"
            onSubmit={ handleSubmit(loginRequest) }>
        <h1 className="login__title">LOGIN BACKOFFICE</h1>
        <div>
          <Field
            placeholder={l('LOGIN_PAGE->USERNAME')}
            name="username"
            component={ LoginFormInput }
            type="text" />
        </div>
        <div>
          <Field
            placeholder={l('LOGIN_PAGE->PASSWORD')}
            name="password"
            component={ LoginFormInput }
            type="password" />
        </div>
        <Button 
          type="submit" 
          text={l('LOGIN_PAGE->LOGIN-ACTION')} 
          classes="btn btn--icon-right btn--is-primary" 
          icon="icon_arrow" 
          display={buttonDisplay} 
          />
        <div className="error">
          { formError && l('LOGIN_PAGE->ERROR-SOMETHING-WRONG') }
        </div>
      </form>
    );
  }
}

Login.propTypes = {
  formError: PropTypes.string,
  handleSubmit: PropTypes.func,
  loginRequest: PropTypes.func,
  submitting: PropTypes.bool,
  params: PropTypes.object
};

Login.contextTypes = {
  locale: PropTypes.string
};

const LoginForm = reduxForm({
  form: 'loginForm',
  fields: ['username', 'password'],
  validate
})(Login);

export default connect(
  (state) => { return state.auth; },
  (dispatch) => bindActionCreators(actionCreators, dispatch)
)(LoginForm);
