import React, { Component, PropTypes } from 'react';

import MainMenu from '../components/MainMenu';
import UserArea from '../components/UserArea';

class MainContainer extends Component {
  
  componentDidMount() {}

  render() {
    return (
      <div className="wrapper">
        <MainMenu />
        <div className="content-wrapper">
          <UserArea />
          <div className="content">
          {this.props.children}
          </div>
        </div>
      </div>
    );
  }
}

MainContainer.propTypes = {
  children: PropTypes.element
};

export default MainContainer;