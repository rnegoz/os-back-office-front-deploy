import React, {Component,PropTypes} from 'react';
import {PieChart, Pie, Cell, Sector, Legend, ResponsiveContainer} from 'Recharts';


const getFormatMidAngle = (midAngle) => {
  const angle = midAngle % 360;
  const temp = 180 * Math.round(angle / 180);
  
  return temp + Math.sign(angle - temp) * Math.max(Math.abs(angle - temp), 60);
};         
const renderActiveShape = (props) => {
  const RADIAN = Math.PI / 180;
  const { cx, cy, midAngle, innerRadius, outerRadius, startAngle, endAngle, fill, payload, percent, value } = props;
  const angle = getFormatMidAngle(midAngle);
  const sin = Math.sin(-RADIAN * angle);
  const cos = Math.cos(-RADIAN * angle);
  const sx = cx + (outerRadius + 10) * Math.cos(-RADIAN * midAngle);
  const sy = cy + (outerRadius + 10) * Math.sin(-RADIAN * midAngle);
  const mx = sx + 16 * cos;
  const my = sy + 16 * sin;
  const ex = mx + (cos > 0 ? 1 : -1) * 16;
  const ey = my;
  const textAnchor = cos > 0 ? 'start' : 'end';
  
  return (
    <g>
      <text x={cx} y={cy} dy={8} textAnchor="middle" fill={fill}>{`${payload.name}:${payload.value}`}</text>
      <Sector
        cx={cx}
        cy={cy}
        innerRadius={innerRadius}
        outerRadius={outerRadius}
        startAngle={startAngle}
        endAngle={endAngle}
        fill={fill}
      />
      <Sector
        cx={cx}
        cy={cy}
        startAngle={startAngle}
        endAngle={endAngle}
        innerRadius={outerRadius + 6}
        outerRadius={outerRadius + 10}
        fill={fill}
      />
      <path d={`M${sx},${sy}L${mx},${my}L${ex},${ey}`} stroke={fill} fill="none"/>
      <circle cx={ex} cy={ey} r={2} fill={fill} stroke="none"/>
      <text x={ex + (cos > 0 ? 1 : -1) * 6} y={ey} textAnchor={textAnchor} fill="#333">{`Votes ${value}`}</text>
      <text x={ex + (cos >= 0 ? 1 : -1) * 6} y={ey} dy={18} textAnchor={textAnchor} fill="#999">
        {`(Rate ${(percent * 100).toFixed(2)}%)`}
      </text>
    </g>
  );
};

class PieChartComponent extends Component {
  constructor() {
    super();
    this.state ={
      activeIndex: -1,
    };
    this.onPieEnter = this.onPieEnter.bind(this);
    this.onPieOut = this.onPieOut.bind(this);
    this.onAreaEnter = this.onAreaEnter.bind(this);
    this.onAreaLeave = this.onAreaLeave.bind(this); 
  }
  onPieEnter(data, index) {
    this.setState({
      activeIndex: index,
    });
  }
  onPieOut(data, index) {
    this.setState({
      activeIndex: -1,
      fillOpacity: 1
    });
  }
  onAreaEnter(data, index) {
    this.setState({
      fillOpacity: 0.7
    });
  }
  onAreaLeave(data, index) {
    this.setState({
      fillOpacity: 1
    });
  }

  render () {
    const COLORS=['#7AC0FF', '#4A4A4A', '#9BD757', '#9B9B9B'];
    //const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042','#16a085','#16a085'];
    return (
      <ResponsiveContainer>
        <PieChart onMouseEnter={this.onPieEnter.bind(this)} onMouseLeave={this.onPieOut.bind(this)} width={800} height={400} margin={{ top: 10, right: 10, bottom: 10, left: 10 }}>
          <Pie activeIndex={this.state.activeIndex} activeShape={renderActiveShape} isAnimationActive={true} data={this.props.data} animationEasing="ease-out"
            cx="50%" 
            cy="50%" 
            innerRadius="50%"
            outerRadius="70%"
            legendType="circle"
            /*paddingAngle={1} startAngle={-135} endAngle={-405}*/ >
            {
            this.props.data.map((entry, index) => <Cell key ={index} fill={COLORS[index % COLORS.length]}
                onMouseEnter={this.onAreaEnter.bind(this, index)}
                onMouseLeave={this.onAreaLeave.bind(this, index)}
                fillOpacity={this.state.fillOpacity} />)
            }
          </Pie>
          <Legend verticalAlign="top" fillOpacity={this.state.fillOpacity}/>
        </PieChart>
      </ResponsiveContainer>
    );

  }
}

export default PieChartComponent;