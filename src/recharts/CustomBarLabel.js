import React, {Component, PropTypes} from 'react';

const RenderBarLabel = (props) => {
    const {
        payload,
        textAnchor,
        fill,
        x,
        y,
        width,
        height,
        colorLabel
    } = props;

    if (payload.value > 0) {
        //var total = payload.reduce((result, entry) => (result + entry.value), 0);
        return (
            <text
                textAnchor={textAnchor}
                fill={colorLabel}
                x={x}
                y={y}
                width={width}
                height={height}
                className="recharts-bar-label">
                {payload.value}
            </text>
        );
    } else {
        return <text></text>;
    }
};

export default RenderBarLabel;