import React, {Component, PropTypes} from 'react';

class RenderTooltip extends Component{
  render() {
    const data  = this.props.data;
      return (
        <div className="custom-tooltip">
          {
            data.map((entry, index,d) => <p key={index} style={{color:entry.color}}>{entry.value} : <span>{this.props.payload[index].value}</span></p>)
          }
        </div>
      );
  }
}

export default RenderTooltip;