import React, {Component,PropTypes} from 'react';
import {LineChart, Line, CartesianGrid, Brush, XAxis, YAxis, Tooltip, Legend, ResponsiveContainer} from 'Recharts';
import CustomTooltip from './CustomTooltip.js';
import CustomLabel from './CustomLabel.js';

class LineChartComponent extends Component {
constructor() {
    super();
    
  }
  render () {
    const colors = this.props.colors;
    var values = this.props.field;
    
    /*** Create payload for CustomTooltip***/
    var arrayOfObjects = [];
    for(var i=0; i<values.length; i++){
      var obj = {};
      for(var j=0; j<values[i].length; j++){
        obj['color'] = colors[i];
        obj['value'] = values[i]; 
      }
      arrayOfObjects.push(obj);
    }
    return (
      <ResponsiveContainer>
        <LineChart layout={this.props.layoutChart} width={this.props.width} height={this.props.height} data={this.props.data}
            margin={this.props.margin}>
          <XAxis dataKey={this.props.xDataKey} label={<CustomLabel Y={false} data={this.props.labelX}/>} />
          <YAxis label={<CustomLabel Y={true} data={this.props.labelY}/>}/>
          <CartesianGrid strokeDasharray="4 0 4" />
          <Tooltip cursor={{fill: 'red', fillOpacity: 0.05}} content={<CustomTooltip data={arrayOfObjects}/>} />
          <Legend height={36} align="center" payload={arrayOfObjects}/*content={<CustomLegend colors={colors} />}*//>
          {
            this.props.cols.map((entry, index,d) => <Line type="monotone" key={index} dataKey={d[index]} stackId={this.props.stackId} stroke={colors[index % colors.length]}/>)
          }
          <Brush />
        </LineChart>
      </ResponsiveContainer>
    );
  }
}

LineChartComponent.propTypes = {
  width: PropTypes.number,
  height: PropTypes.number,
  data: PropTypes.array,
  colors: PropTypes.array,
  margin: PropTypes.object,
  xDataKey: PropTypes.string,
  labelX: PropTypes.string,
  labelY: PropTypes.string,
  stackId: PropTypes.string,
  layoutChart: PropTypes.string,
  field: PropTypes.array,
  cols: PropTypes.array
};

export default LineChartComponent;