import React, {Component, PropTypes} from 'react';
import {AreaChart, Area, CartesianGrid, XAxis, YAxis, Tooltip, Legend, ResponsiveContainer} from 'Recharts';
import CustomLabel from './CustomLabel.js';
import CustomTooltip from './CustomTooltip.js';

class AreaChartComponent extends Component {
  constructor() {
    super();
    
  }
  render () {
    const colors = this.props.colors;
    var values = this.props.field;
    var fields = this.props.data;
    var optionsQuestion = this.props.optionsQuestion;
    
    /*** Create payload for CustomTooltip***/
    var arrayOfObjects = [];
    for(var i=0; i<values.length; i++){
      var obj = {};
      for(var j=0; j<values[i].length; j++){
        obj['color'] = colors[i];
        obj['value'] = values[i]; 
      }
      arrayOfObjects.push(obj);
    }

    return (
      <ResponsiveContainer>
        <AreaChart layout={this.props.layoutChart} width={this.props.width} height={this.props.height} data={this.props.data}
            margin={this.props.margin}>
          <XAxis dataKey={this.props.xDataKey} label={<CustomLabel Y={false} data={this.props.labelX}/>} interval={0} ticks={this.props.xTicks}/>
          <YAxis label={<CustomLabel Y={true} data={this.props.labelY}/>} />
          <CartesianGrid strokeDasharray="4 0 4" />
          <Tooltip cursor={{fill: 'red', fillOpacity: 1}} content={<CustomTooltip data={arrayOfObjects}/>} />
          <Legend align="center" payload={arrayOfObjects}/*content={<CustomLegend colors={colors} />}*//>
          {
            this.props.cols.map((entry, index,d) => <Area key={index} dataKey={d[index]} stackId={this.props.stackId} stroke={colors[index % colors.length]} fill={colors[index % colors.length]}/>)
          }
        </AreaChart>
      </ResponsiveContainer>
    );
  }
}


AreaChartComponent.propTypes = {
  /*width: PropTypes.number,
  height: PropTypes.number,
  data: PropTypes.array,
  margin: PropTypes.object,
  xDataKey: PropTypes.string,
  labelX: PropTypes.string,
  labelY: PropTypes.string,
  barDataKey1: PropTypes.string,
  barDataKey2: PropTypes.string,
  barStackId1: PropTypes.string,
  barStackId2: PropTypes.string,
  barFill1: PropTypes.string,
  barFill2: PropTypes.string,
  layoutChart: PropTypes.string*/
};

export default AreaChartComponent;