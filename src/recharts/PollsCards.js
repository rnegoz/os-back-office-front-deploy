import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';

import capitalize from 'lodash/capitalize';

import requireLocalizations from '../localizations/output/all.js';

import Card from '../components/molecules/Card';
import Button from '../components/atoms/Button';
import Icon from '../components/atoms/Icon';

class PollsCards extends Component {

  constructor(props, context) {
    super(props, context);
    this.l = requireLocalizations(context.locale);
  }
  renderTopics(topics) {
    return topics.join(', ');
  }

  render() {
    // const l = this.l;
    const {data} = this.props;

    return (

      <Card border="dotted" figure backgroundColor="#FFF">

        <div className="card-inner">
          <h3 className="user-card__username">
            Poll Name: {data.poll_name}
          </h3>
          <div>
            <div className="user-card__detail">
              <strong>Creation date: </strong>{data.creation_date}
            </div>
            <div className="user-card__detail">
              <strong>Votes: </strong>{data.total_votes}
            </div>
            <div className="user-card__detail">
              <strong>Voting enabled: </strong>
              {!data.voting_enabled ? 
                (<Icon kind='icon_arrow' size={16} color="red"/>)
              : 
                (<Icon kind='icon_check' size={16} color="green" />)
              }
            </div>
          </div>
        </div>

        <Button type="button" classes="btn btn--is-white btn--icon-left" icon="icon_view" text="VIEW">
          <Link to={`/admin/polls/${data.id}`} >
            VIEW
          </Link>
        </Button>
        
      </Card>
    );
  }
}

PollsCards.PropTypes = {
  data: PropTypes.object.isRequired
};

export default PollsCards;