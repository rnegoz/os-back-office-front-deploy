import React, {Component, PropTypes} from 'react';

const RenderLabel = (props) => {
	const { x, y, width, height,labelX,data,Y} = props;
	var content;
	
	if(Y == true){
	  content = <text x={x + 15} y={y - 25} className="customized-y-axis-label">{data}</text>;
	}else{
	  content = <text x={x + 20 + width} y={y + 5} className="customized-x-axis-label">{data}</text>;
	}
	return (
   		<g>{content}</g>
  	);
};

export default RenderLabel;