import React, {Component, PropTypes} from 'react';
import {BarChart, Bar, CartesianGrid, XAxis, YAxis, Tooltip, Legend, ResponsiveContainer} from 'Recharts';
import CustomLabel from './CustomLabel.js';
import CustomBar from './CustomBar.js';
import CustomTooltip from './CustomTooltip.js';
import CustomBarLabel from './CustomBarLabel.js';

// const renderCustmoizedShape = (props) => {
// const { x, y, width, height, value,colors,cols } =props;
//   return <rect x={x} y={y} width={width} height={height} fill={colors} fillOpacity={value > 3 ? 1 : 0.5}/>;
// }
// const renderVerticalTick = ({ payload, x, y }) => {
//   return (
//     <g transform={`translate(${x}, ${y})`}>
//       <text transform="rotate(-90)" textAnchor="end" alignmentBaseline="central">{payload.value}</text>
//     </g>
//   );
// };

class BarChartComponent extends Component {
  constructor() {
    super();
  }
  render () {
    const colors = this.props.colors;
    var values = this.props.field;
    var fields = this.props.data;
    var optionsQuestion = this.props.optionsQuestion;
    
    /*** Create payload for CustomTooltip***/
    var arrayOfObjects = [];
    for(var i=0; i<values.length; i++){
      var obj = {};
      for(var j=0; j<values[i].length; j++){
        obj['color'] = colors[i];
        obj['value'] = values[i]; 
      }
      arrayOfObjects.push(obj);
    }
    return (
      <ResponsiveContainer>
        <BarChart layout={this.props.layoutChart} width={this.props.width} height={this.props.height} data={this.props.data}
            margin={this.props.margin}>
          <XAxis dataKey={this.props.xDataKey} label={<CustomLabel Y={false} data={this.props.labelX}/>} type={this.props.typeX}  tick={this.props.xTicks} hide={this.props.XAxisHide}/>
          <YAxis dataKey={this.props.yDataKey} label={<CustomLabel Y={true} data={this.props.labelY}/>} type={this.props.typeY} hide={this.props.YAxisHide} domain={[0, 'dataMax +15']}/>
          {(this.props.CartesianGridHide) ? null : <CartesianGrid strokeDasharray="3 3" />}
          <Tooltip cursor={{fill: 'red', fillOpacity: 0.05}} content={<CustomTooltip data={arrayOfObjects}/>} />
          <Legend align="center" payload={arrayOfObjects}/*content={<CustomLegend colors={colors} />}*//>
          {
            this.props.cols.map((entry, index,d) => <Bar shape={<CustomBar layout={this.props.layoutChart}/>} animationEasing="ease" key={index} dataKey={d[index]} stackId={this.props.stackId} fill={colors[index % colors.length]} barSize={35} label={<CustomBarLabel colorLabel={colors[index % colors.length]}/>} />)
          }
        </BarChart>
      </ResponsiveContainer>
    );
  }
}

BarChartComponent.propTypes = {
  /*width: PropTypes.number,
  height: PropTypes.number,
  data: PropTypes.array,
  margin: PropTypes.object,
  xDataKey: PropTypes.string,
  labelX: PropTypes.string,
  labelY: PropTypes.string,
  barDataKey1: PropTypes.string,
  barDataKey2: PropTypes.string,
  barStackId1: PropTypes.string,
  barStackId2: PropTypes.string,
  barFill1: PropTypes.string,
  barFill2: PropTypes.string,
  layoutChart: PropTypes.string*/
};

export default BarChartComponent;