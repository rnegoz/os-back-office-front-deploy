import React, { Component, PropTypes } from 'react';
//import Spinner from './Spinner.js';


class CardChart extends Component {
  constructor() {
    super();
  }
  render () {
    var content;
    if(this.props.loading == 0 || this.props.loading==null){
      content = <p>download...</p>
    } else {
      content = this.props.children;
    }

    return (
      <div className="card-charts" style={{border: `1px ${this.props.border} #CCCCCC`}}>
        <div className="card-inner">
          <h3>{this.props.title}</h3>
          <div className="card-content" >
            {content}
          </div>
        </div>
      </div>
    );

  }
}
CardChart.propTypes = {
  //loading: PropTypes.bool,
  title: PropTypes.string
};

export default CardChart;