import React, {Component, PropTypes} from 'react';
import {BarChart,Rectangle, Bar, Cell, CartesianGrid, XAxis, YAxis, Tooltip, Legend, ResponsiveContainer} from 'Recharts';
import CustomLabel from './CustomLabel.js';
import CustomBar from './CustomBar.js';

// class CustomLegend extends Component{
//   constructor() {
//     super();
    
//   }
//   render() {
//     return (
//       <ul>
//         {
//           this.props.data.map((entry, index) => (
//             <li style={{color:this.props.colors[index % this.props.colors.length]}} key={`item-${index}`}>{entry.name}</li>
//           ))
//         }
//       </ul>
//     );
//   }

// }

// const CustomBar = (props) => {
//     const { stroke, fill, x, y, width, height } = props;

//     if (height > 0) {
//         return <Rectangle
//             x={x}
//             y={y}
//             width={width}
//             height={height}
//             stroke={stroke}
//             fill={fill}
//             radius={[5,5,0,0]}//left-top, right-top, right-bottom, left-bottom when radius is an array
//         />
//     } else {
//         return <path d={getPath(x, y, 0, 0)} stroke="none" fill={fill}/>;
//     }
// };

// const RenderLabel = (props) => {
// const { x, y, width, height,labelX,data,Y} = props;
// var content;
// if(Y == true){
//   content = <text x={x+15} y={y-25} className="customized-y-axis-label">{data}</text>;
// }else{
//   content = <text x={x + 20 + width} y={y} className="customized-x-axis-label">{data}</text>;
// }
//   return (
//    <g>{content}</g>
//   );
// };

class SimpleBarChartComponent extends Component {
  constructor() {
    super();
    
  }
  render () {
    const colors = this.props.colors;
    return (
      <ResponsiveContainer>
        <BarChart layout={this.props.layoutChart} width={this.props.width} height={this.props.height} data={this.props.data}
              margin={this.props.margin}>
         <XAxis dataKey={this.props.xDataKey} label={<CustomLabel Y={false} data={this.props.labelX}/>} />
         <YAxis dataKey={this.props.barDataKey} label={<CustomLabel Y={true} data={this.props.labelY}/>} axisLine={true} tick={true} tickSize={10} domain={[0, 'dataMax +4']}/>
         <CartesianGrid fillOpacity={0.5} stroke="#9B9B9B" strokeDasharray="4 0 4"/>
         <Tooltip />
           <Bar name="Votes" barSize={40} dataKey={this.props.barDataKey} shape={<CustomBar/>} barSize={35} label={true} stackId={this.props.stackId}>
              {
                this.props.data.map((entry, index) => (
                  <Cell cursor="pointer" fill={colors[index % colors.length]} key={`cell-${index}`}/>
              ))
            }
           </Bar>
        </BarChart>
      </ResponsiveContainer>
    );
  }
}


SimpleBarChartComponent.propTypes = {
  /*width: PropTypes.number,
  height: PropTypes.number,
  data: PropTypes.array,
  margin: PropTypes.object,
  xDataKey: PropTypes.string,
  labelX: PropTypes.string,
  labelY: PropTypes.string,
  barDataKey1: PropTypes.string,
  barDataKey2: PropTypes.string,
  barStackId1: PropTypes.string,
  barStackId2: PropTypes.string,
  barFill1: PropTypes.string,
  barFill2: PropTypes.string,
  layoutChart: PropTypes.string*/
};

export default SimpleBarChartComponent;