import React, {Component, PropTypes} from 'react';
import {Rectangle} from 'Recharts';

const RenderBar = (props) => {
  const { stroke, fill, x, y, width, height, layout} = props;
  var radius;
    if (height > 0) {
        return <Rectangle
            x={x}
            y={y}
            width={width}
            height={height}
            stroke={stroke}
            fill={fill}
            radius={(layout == 'vertical') ? [0,5,5,0] : [5,5,0,0]}
        />
    } else {
      return <path d={getPath(x, y, 0, 0)} stroke="none" fill={fill}/>;
    }
};

export default RenderBar;