
## Localizations

We are going to use [L10ns](http://l10ns.org/docs.html) as a tool for both translations and localization.

So please, install the global package:

```
$ npm install l10ns -g
```

This will keep track of the string ids in the code, searching on several levels of the /src folder.

When you'd like to use a new string in your code, include
```import requireLocalizations from '../localizations/output/all.js';```

on the imports section of your component, and define a constructor in your component in this way:
```
constructor(props, context) {
  super(props, context);
  this.l = requireLocalizations(context.locale);
}
```

Your component needs to get access to the `locale` variable inside the app's context, through this code:
```
MyComponent.contextTypes = {
  locale: PropTypes.string
};
```

To use the translation service is through the `l` function that we have defined in the constructor. This could be done in several fashions, so please check the documentation at [http://l10ns.org/docs.html](http://l10ns.org/docs.html)

The easiest way to define a translation string id would be `{ l('WELCOME_PAGE->ERROR') }` but there are lots of possibilities with the use of internationalization for dates, currency, etc .. 

Please group up your string ids according to the component they are in or their functionality. 
For the most common, reusable strings, we could come up with a default namespace like this:
```
{ l('DEFAULT->CANCEL') }
{ l('DEFAULT->ACCEPT') }
```
Namespaces accept all kinds of strings, so names like `DEFAULT::CANCEL` or `DEFAULT|CANCEL` are also allowed. Please be consistent.

Once you have written your string ids in this way, you should run the appropiate node processes to update the strings collection and the json files. At the moment of this write-up, `l10ns` has problems parsing ES6 code, so we are going to use a [dictionary](./dictionary.js) file to set up all the different string ids and to parse them from there.

First, run `$ l10ns update`. This will create / update each language's json files. Currently we have `es-ES.json` and `en-US.json` files set, with the `en-US` as the default language for the platform. 

Then use the built-in interface to write the translations of the new string ids by running `$ l10ns interface` 

Once the translations are in place, you can either click on the `Compile` button on the interface or run the `$ l10ns compile` command on your CLI .. 

This would update the `src/localizations/output/*.js` files for you to use on your project.


