import { browserHistory } from 'react-router';
import merge from 'lodash/merge';
import isEmpty from 'lodash/isEmpty';
import axios from 'axios';
import jwtDecode from 'jwt-decode';

import initialState from './initialState';
import setAuthToken from '../utils/setAuthToken';

import { ENDPOINTS } from '../constants/constants';

// Actions
const LOGIN_FAIL = 'login/LOGIN_FAIL';
const LOGIN_SUCCESS = 'login/LOGIN_SUCCESS';
const LOGOUT= 'login/LOGOUT';

// Reducer
export default function reducer(state = initialState.auth, action) {

  switch(action.type) {

    case LOGIN_FAIL:
      return merge({}, state, { isAuthenticated: isEmpty(action.user), formError: action.message, user: '' });

    case LOGIN_SUCCESS:
      return merge({}, state, { isAuthenticated: !isEmpty(action.user), formError: '', user: action.payload });

    case LOGOUT:
      return merge({}, state, {isAuthenticated: false, formError: '', user: ''});

    default:
      return state;
  }
}

// Action Creators
export function loginFail(message = '') {
  return {
    type: LOGIN_FAIL,
    message
  };
}

export function loginSuccess(token) {
  return {
    type: LOGIN_SUCCESS,
    payload: token
  };
}

export function logOut() {
  localStorage.removeItem('jwtToken');
  setAuthToken(false);
  return {
    type: LOGOUT,
    payload: ''
  };
}

export function loginRequest(credentials = { username: '', password: '' }) {
  return (dispatch) => {
    const urlAPI =  `${ENDPOINTS.LOGIN}/authenticate`;
    return axios.put(urlAPI, credentials, {responseType: 'json'})
      .then((response) => {
        const token = response.headers.authorization;
        localStorage.setItem('jwtToken', token);
        setAuthToken(token);
        // TECHDEBT read permission 'canAccessBO' in the JWT
        // and decide what to do with the user
        dispatch(loginSuccess(jwtDecode(token)));
      })
      .then(() => {
        dispatch(() => {
          browserHistory.push('/admin');
        });
      })
      .catch((err) => {
        const msg = (err.data && err.data.message);
        console.error(err, msg);
        dispatch(loginFail(msg));
      });
  };
}
