import { combineReducers } from 'redux';

import { routerReducer } from 'react-router-redux';
import { reducer as formReducer } from 'redux-form';

import loginReducer from './loginReducer';
import settingsReducer from './settingsReducer';
import userDirectoryReducer from './userDirectoryReducer';

const rootReducer = combineReducers({
  auth: loginReducer,
  users: userDirectoryReducer,
  settings: settingsReducer,
  routing: routerReducer,
  form: formReducer
});

export default rootReducer;
