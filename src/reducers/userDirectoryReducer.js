import merge from 'lodash/merge';
import assign from 'lodash/assign';
import find from 'lodash/find';
import findIndex from 'lodash/findIndex';
import axios from 'axios';

import initialState from './initialState';
import { USERS_OFFSET, USERS_PER_PAGE } from '../constants/constants';

// Actions
const FETCH_USERS = 'users/FETCH_USERS';
const FILTER_BY = 'users/FILTER_BY';
const SEARCH_USER = 'users/SEARCH_USER';
const LABEL_USER = 'users/LABEL_USER';
const BLOCK_USER = 'users/BLOCK_USER';
const UNBLOCK_USER = 'users/UNBLOCK_USER';
const SUSPEND_USER = 'users/SUSPEND_USER';
const UNSUSPEND_USER = 'users/UNSUSPEND_USER';
const LOADING = 'users/LOADING';
const ERROR = 'users/ERROR';

function findProfile(profiles, user_id) {
  let index = findIndex(profiles, {'user_id': user_id});
  let userObject = find(profiles, {'user_id': user_id});
  return {index, userObject};
}

// Reducer
export default function reducer(state = initialState.users, action) {

  switch(action.type) {

    case FETCH_USERS:
      return assign({}, state, {profiles: action.profiles});

    case FILTER_BY:
      return state;

    case SEARCH_USER:
      return assign({}, state, {profiles: action.profiles});

    case LABEL_USER: {
      let {index, userObject} = findProfile(state.profiles, action.user_id);
      let newUserObj = merge({}, userObject, {label: action.value});

      return {
        ...state,
        profiles: [
          ...state.profiles.slice(0, index),
          newUserObj,
          ...state.profiles.slice(index + 1)
        ]
      };
    }

    case BLOCK_USER: {
      let {index, userObject} = findProfile(state.profiles, action.user_id);
      let newUserObj = merge({}, userObject, {user_isBlocked: true});

      return {
        ...state,
        profiles: [
          ...state.profiles.slice(0, index),
          newUserObj,
          ...state.profiles.slice(index + 1)
        ]
      };
    }
    case UNBLOCK_USER: {
      let {index, userObject} = findProfile(state.profiles, action.user_id);
      let newUserObj = merge({}, userObject, {user_isBlocked: false});

      return {
        ...state,
        profiles: [
          ...state.profiles.slice(0, index),
          newUserObj,
          ...state.profiles.slice(index + 1)
        ]
      };
    }

    case SUSPEND_USER: {
      let {index, userObject} = findProfile(state.profiles, action.user_id);
      let newUserObj = merge({}, userObject, {user_isSuspended: true});

      return {
        ...state,
        profiles: [
          ...state.profiles.slice(0, index),
          newUserObj,
          ...state.profiles.slice(index + 1)
        ]
      };
    }
    case UNSUSPEND_USER: {
      let {index, userObject} = findProfile(state.profiles, action.user_id);
      let newUserObj = merge({}, userObject, {user_isSuspended: false});

      return {
        ...state,
        profiles: [
          ...state.profiles.slice(0, index),
          newUserObj,
          ...state.profiles.slice(index + 1)
        ]
      };
    }

    case LOADING:
      return merge({}, state, { isLoading: action.isLoading, hasError: '' });

    case ERROR:
      return merge({}, state, { hasError: action.hasError });

    default:
      return state;
  }
}

// Action Creators
export function fetchUsers(offset = USERS_OFFSET, limit = USERS_PER_PAGE) {
  return (dispatch) => {
    dispatch(isLoading(true));
    const urlAPI = `/json/users.json?offset=${offset}&limit=${limit}`;
    return axios.get(urlAPI, {offset, limit}, {responseType: 'json'})
      .then((response) => {
        const profiles = response.data.profiles;
        dispatch({
          type: FETCH_USERS,
          profiles
        });
        dispatch(isLoading(false));
      })
      .catch((err) => {
        dispatch(isLoading(false));
        const msg = (err && err.message);
        dispatch(hasError(msg));
      });
  };
}

export function filterBy(payload) {
  return {type: FILTER_BY, payload};
}

export function searchUser(query = '', offset = USERS_OFFSET, limit = USERS_PER_PAGE) {
  return (dispatch) => {
    dispatch(isLoading(true));
    const urlAPI =  `/json/search.json`;
    return axios.get(urlAPI, {offset, limit}, {responseType: 'json'})
      .then((response) => {
        const profiles = response.data.profiles;
        dispatch({
          type: SEARCH_USER,
          profiles
        });
        dispatch(isLoading(false));
      })
      .catch((err) => {
        dispatch(isLoading(false));
        const msg = (err && err.message);
        dispatch(hasError(msg));
      });
  };
}

export function labelUser(user_id, value) {
  return (dispatch) => {
    dispatch(isLoading(true));

    dispatch({type: LABEL_USER,
      user_id,
      value
    });

    // const urlAPI =  `${ENDPOINTS.USERS}/label`;
    // return axios.put(urlAPI, {user_id, value}, {responseType: 'json'})
    //   .then(() => {
    //     dispatch({type: LABEL_USER,
    //        user_id,
    //        value
    //     });
    //   })
    //   .then(() => {
    //     dispatch(isLoading(false));
    //   })
    //   .catch((err) => {
    //     dispatch(isLoading(false));
    //     const msg = (err && err.message);
    //     dispatch(hasError(msg));
    //   });
  };
}

export function blockUser(user_id, reason) {
  return (dispatch) => {
    dispatch(isLoading(true));

    dispatch({type: BLOCK_USER,
      user_id,
      reason
    });

    // const urlAPI =  `${ENDPOINTS.USERS}/block`;
    // return axios.put(urlAPI, {user_id, reason}, {responseType: 'json'})
    //   .then(() => {
    //     dispatch({type: BLOCK_USER,
    //       user_id,
    //       reason
    //     });
    //   })
    //   .then(() => {
    //     dispatch(isLoading(false));
    //   })
    //   .catch((err) => {
    //     dispatch(isLoading(false));
    //     const msg = (err && err.message);
    //     dispatch(hasError(msg));
    //   });
  };
}

export function unblockUser(user_id) {
  return (dispatch) => {
    dispatch(isLoading(true));

    dispatch({type: UNBLOCK_USER,
      user_id
    });
  };

}

export function suspendUser(user_id, reason, period) {
  return (dispatch) => {
    dispatch(isLoading(true));

    dispatch({type: SUSPEND_USER,
      user_id,
      reason,
      period
    });

    // const urlAPI =  `${ENDPOINTS.USERS}/suspend`;
    // return axios.put(urlAPI, {user_id, reason, period}, {responseType: 'json'})
    //   .then(() => {
    //     dispatch({type: SUSPEND_USER,
    //       user_id,
    //       reason,
    //       period
    //     });
    //   })
    //   .then(() => {
    //     dispatch(isLoading(false));
    //   })
    //   .catch((err) => {
    //     dispatch(isLoading(false));
    //     const msg = (err && err.message);
    //     dispatch(hasError(msg));
    //   });
  };
}

export function unsuspendUser(user_id) {
  return (dispatch) => {
    dispatch(isLoading(true));

    dispatch({type: UNSUSPEND_USER,
      user_id
    });
  };
}

export function isLoading(status) {
  return {
    type: LOADING,
    isLoading: !!status
  };
}

export function hasError(msg = '') {
  return {
    type: ERROR,
    hasError: msg
  };
}
