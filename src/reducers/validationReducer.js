import merge from 'lodash/merge';
import axios from 'axios';

import initialState from './initialState';
import { ENDPOINTS } from '../constants/constants';

// Actions
const PLACEHOLDER = 'validations/PLACEHOLDER';

// Reducer
export default function reducer(state = initialState.validations, action) {

  switch(action.type) {

    default:
      return state;
  }
}

// Action Creators
export function fn(payload) {
  return {type: PLACEHOLDER, payload};
}
