export default {
  locale: 'en-US',
  users: {
    profiles: [],
    isLoading: false,
    hasError: ''
  },
  content: {},
  profiles: {},
  consultations: {},
  analysis: {},
  polls: {},
  audits: {},
  validations: {},
  tenant: {
    tenantId: '',
    tenantType: 0,
    tenantName: '',
    web: '',
    db: ''
  },
  user: {
    id: undefined,
    name: '',
    avatar: ''
  },
  auth: {
    isAuthenticated: false,
    user: {},
    token: '',
    formError: ''
  },
  settings: {
    background: '',
    logo: '',
    mainMessage: '',
    mainColor: '#dedede'
  },
  permissions: {}
};
