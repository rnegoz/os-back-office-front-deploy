import merge from 'lodash/merge';

import initialState from './initialState';

// Actions types
const CHANGE_LOGO = 'settings/CHANGE_LOGO';
const CHANGE_BACKGROUND = 'settings/CHANGE_BACKGROUND';
const CHANGE_MESSAGE = 'settings/CHANGE_MESSAGE';
const CHANGE_MAIN_COLOR = 'settings/CHANGE_MAIN_COLOR';

// Reducer
export default function reducer(state = initialState.settings, action) {
  switch(action.type) {
  	case CHANGE_LOGO:
      return merge({}, state, {logo: action.url});
    case CHANGE_BACKGROUND:
      return merge({}, state, {background: action.url});
    case CHANGE_MESSAGE:
      return merge({}, state, {mainMessage: action.message});
    case CHANGE_MAIN_COLOR:
      return merge({}, state, {mainColor: action.color});
    default:
      return state;
  }
}

// Action Creators
export function changeLogo(url) {
  return {type: CHANGE_LOGO, url};
}

export function changeBackground(url) {
  return {type: CHANGE_BACKGROUND, url};
}

export function changeMessage(message) {
  return {type: CHANGE_MESSAGE, message};
}

export function changeMainColor(color) {
  return {type: CHANGE_MAIN_COLOR, color};
}