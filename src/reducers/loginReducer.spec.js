import { expect } from 'chai';
import merge from 'lodash/merge';

import loginReducer from './loginReducer';

const validUsername = 'luke_skywalker@laresistance.com';
const validPassword = 'WhosUrDaddy@619';
const initialState = {
    username: {
      value: '',
      error: ''
    },
    password: {
      value: '',
      error: ''
    }
};

function getState(overrides) {
  return merge({}, initialState, overrides);
}
