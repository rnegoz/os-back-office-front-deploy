import merge from 'lodash/merge';
import axios from 'axios';

import initialState from './initialState';
import { ENDPOINTS } from '../constants/constants';

// Actions
const PLACEHOLDER = 'polls/PLACEHOLDER';

// Reducer
export default function reducer(state = initialState.polls, action) {

  switch(action.type) {

    default:
      return state;
  }
}

// Action Creators
export function fn(payload) {
  return {type: PLACEHOLDER, payload};
}
