const HOSTS = {
  LOCAL: '',
  TNTMANAGER: 'http://ec2-52-211-126-93.eu-west-1.compute.amazonaws.com:8081',
  AUTHMAN: 'http://ec2-52-211-126-93.eu-west-1.compute.amazonaws.com:8082',
  CITY: '',
  MOCKAPI: 'http://localhost:3000',
  APIv1: 'http://api.civiciti.com/v1/'
};

export const ENDPOINTS = {
  TENANT: HOSTS.TNTMANAGER + '/tenant',
  LOGIN: HOSTS.AUTHMAN + '/auth',
  USERS: HOSTS.MOCKAPI + '/_users'
};

export const COLORS = ['#ff0000','#4a4a4a','#7ac229','#71b2ed','#f57223','#bd10e0'];

export const LANGUAGES = ['en-US','es-ES'];
export const USERS_OFFSET = 0;
export const USERS_PER_PAGE = 25;

