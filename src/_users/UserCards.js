import React, { Component, PropTypes } from 'react';
import capitalize from 'lodash/capitalize';
import cx from 'classnames';

import requireLocalizations from '../localizations/output/all.js';

import Avatar from '../components/atoms/Avatar';
import Button from '../components/atoms/Button';
import Icon from '../components/atoms/Icon';
import Label from '../components/atoms/Label';
import ButtonGroup from '../components/molecules/ButtonGroup';

class UserCards extends Component {

  constructor(props, context) {
    super(props, context);
    this.state = {isOpen: false};
    this.l = requireLocalizations(context.locale);
  }

  onSuspend = (event) => {
    event.preventDefault();
    const {user_id} = this.props.profile;
    this.props.handleSuspend(user_id);
  };

  onUnsuspend = (event) => {
    event.preventDefault();
    const {user_id} = this.props.profile;
    this.props.handleSuspend(user_id, 'unsuspend');
  };

  onBlock = (event) => {
    event.preventDefault();
    const {user_id} = this.props.profile;
    this.props.handleBlock(user_id);
  };

  onUnblock = (event) => {
    event.preventDefault();
    const {user_id} = this.props.profile;
    this.props.handleBlock(user_id, 'unblock');
  };

  onLabelSelect = (value) => {
    const {user_id} = this.props.profile;
    this.props.handleLabelUser(user_id, value);
  };

  toggleCard = () => {
    let change = !this.state.isOpen;
    this.setState({ isOpen: change });
  };

  renderTopics(topics) {
    return topics.join(', ');
  }

  render() {
    // const l = this.l;
    const {profile} = this.props;
    let isOpenClass = cx('user-card', {'user-card__detailed': this.state.isOpen});

    return (

      <div className={isOpenClass}>
        <div className="flex-row toggle-open" onClick={this.toggleCard}>
          <Avatar src={profile.avatar} />
          <div className="user-card__data">
            <h3 className="user-card__username">
              {profile.fullname}
              <Label size={16} type={profile.label} />
              <span className="languages">{profile.languages}</span>
            </h3>
            <div>
              <div className="user-card__detail">
                <strong>User ID: </strong>{profile.user_id}
              </div>
              <div className="user-card__detail">
                <strong>email: </strong>{profile.email} <Icon kind="icon_check" size={12} color="#6ab01d" />
              </div>
            </div>

            { this.state.isOpen ? (
            <div>
              <div className="user-card__detail">
                <strong>Member since: </strong>{profile.member_since}
              </div>
              <div className="user-card__detail">
                <strong>Last seen: </strong>{profile.last_seen}
              </div>
            </div>
            ) : null }

            { this.state.isOpen ? (
            <div>
              <div className="user-card__detail">
                <strong>Activity level: </strong>{profile.activity_level}
              </div>
              <div className="user-card__detail">
                <strong>DNI / Passport: </strong>{profile.dni}
              </div>
            </div>
            ) : null }
          </div>

          { this.state.isOpen ? (
          <div className="user-card__other">
            <ul>
              <li><Icon kind="icon_mobile" size={16} color="#CCC" /> {profile.phone} <Icon kind="icon_check" size={12} color="#6ab01d" /></li>
              <li><Icon kind="icon_passport" size={16} color="#CCC" /> Passport confirmed <Icon kind="icon_check" size={12} color="#6ab01d" /></li>
              <li><Icon kind="icon_genre" size={16} color="#CCC" /> {capitalize(profile.gender)}</li>
              <li><Icon kind="icon_birthday" size={16} color="#CCC" /> {profile.birthdate}</li>
            </ul>
          </div>
          ) : null }

        </div>

        { this.state.isOpen ? (
        <div className="user-card__topics">
          <div>
            <strong>Topics: </strong>
            {this.renderTopics(profile.topics)}
          </div>
          <div>
            <ul className="activity-list">
              <li><strong>{profile.activity.issues}</strong> issues</li>
              <li><strong>{profile.activity.polls}</strong> active polls</li>
              <li><strong>{profile.activity.ideas}</strong> ideas</li>
              <li><strong>{profile.activity.appeals}</strong> appeals</li>
              <li><strong>{profile.activity.crowdfunds}</strong> active crowdfunds</li>
              <li><strong>{profile.activity.sponsorship}</strong> sponsorship</li>
            </ul>
          </div>
        </div>
        ) : null }

        { this.state.isOpen ? (
        <div className="user-card__actions">
          {profile.label !== 'mayor' ? (
          <div className="actions-labels">
            <span>Labels</span>
            <ButtonGroup 
              handleSelection={this.onLabelSelect}
              buttonLabels={['neutral', 'friendly', 'problematic']} 
              selectedLabel={profile.label}>
                <Label size={24} type="neutral" />
                <Label size={24} type="friendly" />
                <Label size={24} type="problematic" />
            </ButtonGroup>
          </div>
          ) : null }
          
          <div className="actions-buttons">
          { !profile.user_isSuspended ? 
            (<Button 
              type="button" 
              text="SUSPEND USER"
              classes="btn btn--icon-right btn--is-positive" 
              icon="icon_time" 
              handleClick={this.onSuspend}
              />)
          :
            (<Button 
              type="button" 
              text="UNSUSPEND USER"
              classes="btn btn--icon-right btn--is-undo" 
              icon="icon_time" 
              handleClick={this.onUnsuspend}
              />)
          }
          { !profile.user_isBlocked ?
            (<Button 
              type="button" 
              text="BLOCK USER"
              classes="btn btn--icon-right btn--is-negative" 
              icon="icon_user_block" 
              handleClick={this.onBlock}
              />)
          :
            (<Button 
              type="button" 
              text="UNBLOCK USER"
              classes="btn btn--icon-right btn--is-undo" 
              icon="icon_user_block" 
              handleClick={this.onUnblock}
              />)
          }
          </div>
        </div>
        ) : null }
      </div>

    );
  }
}

UserCards.propTypes = {
  profile: PropTypes.object.isRequired,
  handleSuspend: PropTypes.func.isRequired,
  handleBlock: PropTypes.func.isRequired,
  handleLabelUser: PropTypes.func.isRequired
};

UserCards.contextTypes = {
  locale: PropTypes.string
};

export default UserCards;