import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import SkyLight from 'react-skylight';
import ModalStyles from '../utils/ModalStyles';
import Paginate from 'react-paginate';
import Icon from '../components/atoms/Icon.js';

import Loading from '../components/atoms/Loading';
import SearchField from '../components/molecules/SearchField';
import UserCards from './UserCards';
import UserSuspend from './UserSuspend';
import UserBlock from './UserBlock';

import * as actionCreators from '../reducers/userDirectoryReducer';
import requireLocalizations from '../localizations/output/all.js';

class UsersPage extends Component {

  constructor(props, context) {
    super(props, context);
    this.state = {selectedUser: undefined};
    this.l = requireLocalizations(context.locale);
  }

  componentDidMount() {
    this.props.actions.fetchUsers();
  }

  handlePagination = (page) => {
    this.props.actions.fetchUsers(25 * page.selected);
  }

  delegatedSearch = (query) => {
    if(query === '') {
      this.props.actions.fetchUsers();
    } else {
      this.props.actions.searchUser(query);
    }
  };

  // Delegated show modals for Block and Suspend users
  // or calls the corresponsing action creator
  delegatedUserBlock = (user_id, action = 'block') => {
    this.state = {selectedUser: user_id};
    action === 'block' ?
      this.refs.modalBlock.show()
      :
      this.props.actions.unblockUser(user_id);
  };
  delegatedUserSuspend = (user_id, action = 'suspend') => {
    this.state = {selectedUser: user_id};
    action === 'suspend' ?
      this.refs.modalSuspend.show()
      :
      this.props.actions.unsuspendUser(user_id);
  };

  delegatedLabelUser = (user_id, value) => {
    this.props.actions.labelUser(user_id, value);
  };

  // Delegated Block and Suspend confirm dialogs on modals
  delegatedConfirmBlock(reason) {
    this.props.actions.blockUser(this.state.selectedUser, reason);
  }

  delegatedConfirmSuspend(reason, period) {
    this.props.actions.suspendUser(this.state.selectedUser, reason, period);
  }

  render() {
    // const l = this.l;
    const {users} = this.props;
    const {overlayStyles, dialogStyles, closeButtonStyle} = ModalStyles;

    return (
      <section className="tmpl-userdirectory">
        <h1>User Directory</h1>
        <SearchField handleSearch={this.delegatedSearch} />

        { users.profiles && users.profiles.length > 0 ?
          users.profiles.map(profile => {
            return (<UserCards
                    key={profile.user_id}
                    profile={profile}
                    handleSuspend={this.delegatedUserSuspend}
                    handleBlock={this.delegatedUserBlock}
                    handleLabelUser={this.delegatedLabelUser} />);
          })
          :
          <Loading size="34px" />
        }

        <SkyLight
          hideOnOverlayClicked
          ref="modalSuspend"
          title="Do you really want to suspend a user?"
          overlayStyles={overlayStyles}
          dialogStyles={dialogStyles}
          closeButtonStyle={closeButtonStyle}>
          <UserSuspend
            handleConfirm={this.delegatedConfirmSuspend.bind(this)}
            ref={(ref) => this.mSuspend = ref} />
        </SkyLight>

        <SkyLight
          hideOnOverlayClicked
          ref="modalBlock"
          title="Do you really want to block a user?"
          overlayStyles={overlayStyles}
          dialogStyles={dialogStyles}
          closeButtonStyle={closeButtonStyle}>
          <UserBlock
            handleConfirm={this.delegatedConfirmBlock.bind(this)}
            ref={(ref) => this.mBlock = ref} />
        </SkyLight>

        <Paginate
          pageNum={50} // this should be total # of pages
          pageRangeDisplayed={0}
          marginPagesDisplayed={0}
          breakLabel='of 50'
          clickCallback={this.handlePagination}
          previousLabel={<Icon kind="icon_arrow" color="#FFF" size={12} />}
          nextLabel={<Icon kind="icon_arrow" color="#FFF" size={12} />}
          containerClassName="paginate--container"
          previousClassName="paginate--link-prev"
          activeClassName="paginate--pg-active"
          nextClassName="paginate--link-next"
        />
      </section>
    );
  }
}

UsersPage.contextTypes = {
  locale: PropTypes.string
};

UsersPage.propTypes = {
  users: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {
    users: state.users
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actionCreators, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UsersPage);
