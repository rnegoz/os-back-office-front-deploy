import React, { Component, PropTypes } from 'react';

import Button from '../components/atoms/Button';
// import Icon from '../components/atoms/Icon';

class UserSuspend extends Component {

  constructor(props) {
    super(props);
    this.confirmSuspend = this.confirmSuspend.bind(this);
    this.onReasonChanged = this.onReasonChanged.bind(this);
    this.onPeriodChanged = this.onPeriodChanged.bind(this);
    this.state = { 
      inputReason: '',
      inputPeriod: ''
    };
  }

  onReasonChanged(event) {
    const reason = event.currentTarget.value;
    this.setState({ inputReason: reason });
  }

  onPeriodChanged(event) {
    const period = event.currentTarget.value;
    this.setState({ inputPeriod: period });
  }

  confirmSuspend(event) {
    event.preventDefault();
    if(this.state.inputReason === '') {
      return false;
    }
    const reason = this.state.inputReason;
    const period = this.state.inputPeriod;
    this.props.handleConfirm(reason, period);
  }

  render() {
      return (
        <div className="suspend-modal">
          <div className="suspend-modal__choices">
            <div className="suspend-modal__content">
              <ul>
                <li>
                  <p>To do this, you must choose a reason.</p>
                </li>
                <li>
                  <input onChange={this.onReasonChanged} type="radio" name="reason" value="offensive"/> 
                  <label>Offensive use of the platform</label>
                </li>
                <li>
                  <input onChange={this.onReasonChanged} type="radio" name="reason" value="fake"/>
                  <label>Seems to be a fake user</label>
                </li>
              </ul>
            </div>
            <div className="suspend-modal__content">
              <ul>
                <li>
                  How long do you wish to suspend this user?
                </li>
                <li>
                  <input onChange={this.onPeriodChanged} type="radio" name="time" value="1"/>
                  <label>1 day</label>
                </li>
                <li>
                  <input onChange={this.onPeriodChanged} type="radio" name="time" value="365"/>
                  <label>1 year</label>
                </li>
              </ul>
            </div>
          </div>
          <div className="suspend-modal__actions">
            <Button 
              type="button" 
              text="CONFIRM AND SEND NOTIFICATION"
              classes="btn btn--icon-right btn--is-positive" 
              icon="icon_send"
              handleClick={this.confirmSuspend}
              />
            <span className="disclaimer">
              <sup>*</sup> If you confirm, the user will recieve a notification with your reasons.
            </span>
          </div>
        </div>
      );
  }
}

UserSuspend.displayName = 'UserSuspend';

UserSuspend.propTypes = {
    className: PropTypes.string,
    handleConfirm: PropTypes.func.isRequired
};

export default UserSuspend;
