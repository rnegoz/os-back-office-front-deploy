import React, { Component, PropTypes } from 'react';

import Button from '../components/atoms/Button';
// import Icon from '../components/atoms/Icon';

class UserBlock extends Component {

  constructor(props) {
    super(props);
    this.onReasonChanged = this.onReasonChanged.bind(this);
    this.confirmBlock = this.confirmBlock.bind(this);
    this.state = { inputReason: '' };
  }

  onReasonChanged(event) {
    const reason = event.currentTarget.value;
    this.setState({ inputReason: reason });
  }

  confirmBlock(event) {
    event.preventDefault();
    if(this.state.inputReason === '') {
      return false;
    }
    const reason = this.state.inputReason;
    this.props.handleConfirm(reason);
  }

  render() {
      return (
        <div className="block-modal">
          <div className="block-modal__choices">
            <div className="block-modal__content">
              <ul>
                <li>
                  <p>To do this, you must choose a reason.</p>
                </li>
                <li>
                  <label></label>
                  <input onChange={this.onReasonChanged} type="radio" name="reason" value="offensive"/> Offensive use of the platform
                </li>
                <li>
                  <label></label>
                  <input onChange={this.onReasonChanged} type="radio" name="reason" value="inactive"/> Beign inactive for more than a year
                </li>
                <li>
                  <label></label>
                  <input onChange={this.onReasonChanged} type="radio" name="reason" value="fake"/> Seems to be a fake user
                </li>
                <li>
                  <label></label>
                  <input onChange={this.onReasonChanged} type="radio" name="reason" value="spam"/> Using the platform for commercial purposes
                </li>
              </ul>
            </div>
          </div>
          <div className="block-modal__actions">
            <Button 
              type="button" 
              text="CONFIRM AND SEND NOTIFICATION"
              classes="btn btn--icon-right btn--is-positive" 
              icon="icon_send"
              handleClick={this.confirmBlock}
              />
              <span className="disclaimer">
                <sup>*</sup> If you confirm, the user will recieve a notification with your reasons.
              </span>
            </div>
        </div>
      );
  }
}

UserBlock.displayName = 'UserBlock';

UserBlock.propTypes = {
    className: PropTypes.string,
    handleConfirm: PropTypes.func.isRequired
};

export default UserBlock;
