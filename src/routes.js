import React from 'react';
import { Route, IndexRoute } from 'react-router';

import App from './containers/App';
import LoginPage from './components/LoginPage';
import MainContainer from './containers/MainContainer';
import WelcomePage from './containers/WelcomePage';
import UsersPage from './_users/UsersPage';
import ModerationPage from './containers/ModerationPage';
import ProfilesPage from './containers/ProfilesPage';
import AuditPage from './containers/AuditPage';
import ConsultationPage from './containers/ConsultationPage';
import SentimentAPage from './containers/SentimentAPage';
import PollsPage from './containers/PollsPage';
import PollDetail from './containers/PollDetail';
import ValidationPage from './containers/ValidationPage';
import SettingsPage from './_settings/SettingsPage';
import SettingsPreviewPage from './_settings/SettingsPreviewPage';
import PermissionsPage from './_permissions/PermissionsPage';

// import requireAuth from './utils/requireAuth';

export default (
  <Route path="/" component={App}>
    <IndexRoute component={LoginPage} />
    <Route path="/login" component={LoginPage} />
    <Route path="/admin" component={MainContainer}>
      <IndexRoute component={WelcomePage} />
      <Route path="settings" component={SettingsPage} />
      <Route path="permissions" component={PermissionsPage} />
      <Route path="users" component={UsersPage} />
      <Route path="moderation" component={ModerationPage} />
      <Route path="profiles" component={ProfilesPage} />
      <Route path="audit" component={AuditPage} />
      <Route path="consultation" component={ConsultationPage} />
      <Route path="sentiment" component={SentimentAPage} />
      <Route path="polls" component={PollsPage} />
        <Route path="polls/:pollId" component={PollDetail} />
      <Route path="validation" component={ValidationPage} />
    </Route>
    <Route path="admin/settings/preview" component={SettingsPreviewPage} />
  </Route>
);
