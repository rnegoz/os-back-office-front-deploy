# TECH DEBT

Back Office:
------------
[ ] Unit testing all components
[ ] Refactor to use the new React Router 4
[ ] Use Redux Reselect for queries and filters
[ ] Add a method to change language -> cookies/LocalStorage
[ ] Translation to Spanish using l10ns ( UsersPage, SettingsPage, AdvPolls )
[ ] Move /style/imgs to /imgs 

	BO (S)ettings & (P)review: 
	--------------------------
	[ ] Move CSS to styles file (S+P)
	[ ] Rename class with BEM system (S+P)
	[ ] Make Upload of files (S)
	[ ] Create a modal box to crop & move images (S)
	[ ] Refactor Settings Page to use real API endpoints
	[ ] Improve Responsive (or stablish responsive grid system) (S+P)

  BO Users Directory:
  -------------------
  [ ] Refactor to use real API endpoints

  BO Advanced Polls:
  --------------------
  [ ] Move all components to a /src/_advpolls folder
  [ ] Refactor to use real API endpoints


[x] Higher Order component for Authentication
[x] Update SVG Icon component (https://github.com/blueberryapps/react-svg-icon-generator)
[x] Implement JWT token passing on all axios calls (via LocalStorage)
[x] Button component with inner states (static, loading, error)
[x] Update README with current dependencies and ones comming from Consultations / Polls
[x] Use Ducks file approach for Redux (https://github.com/erikras/ducks-modular-redux)
[x] Reorganize component tree in regards of Components and functionality

Results:
--------
[ ] Merge Results into BO
[x] Webpack build script

Consultations:
--------------
[ ] Merge Consultations into BO
[x] Webpack build script

Add or delete whatever we left as a Technical Debt into OS Backoffice & Results
