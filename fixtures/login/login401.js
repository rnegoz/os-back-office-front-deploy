var { ENDPOINTS } = require('../../src/constants/constants');

module.exports = function (mock) {
  mock.onPut(ENDPOINTS.LOGIN).reply(401, {
   'status': '401',
   'code': '401',
   'message': 'Access denied.'
  });
};
