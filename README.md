#OS Backoffice

We are taking React Slingshot, a comprehensive starter kit for rapid application development using React, as a base boilerplate for the development of our platform. 

Clone the repo with `git clone git@bitbucket.org:engineering_os/os-back-office-front.git`. 
This would create an `os-back-office-front` folder. 

```
$ cd os-back-office-front
$ npm install
```

To run the dev server, and start coding, you can `$ npm start` and the webserver would open the application in your default browser. When doing development with this kit, this command will continue watching all your files. Every time you hit save the code is rebuilt, linting runs, and tests run automatically. 
Note: You can use `$npm start -s` The -s flag is optional. It enables silent mode which suppresses unnecessary messages during the build.

##Initial Machine Setup
1. **Install [Node 4.0.0 or greater](https://nodejs.org)** - (5.0 or greater is recommended for optimal build performance). Need to run multiple versions of Node? Use [nvm](https://github.com/creationix/nvm).
2. **Install [Git](https://git-scm.com/downloads)**. 
3. **Install [React developer tools](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi?hl=en) and [Redux Dev Tools](https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd?hl=en)** in Chrome. (Optional, but helpful. The latter offers time-travel debugging.)

**On a Mac**

* You are all set.
 
**On Linux:**  

 * Run this to [increase the limit](http://stackoverflow.com/questions/16748737/grunt-watch-error-waiting-fatal-error-watch-enospc) on the number of files Linux will watch. [Here's why](https://github.com/coryhouse/react-slingshot/issues/6).    
`echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p` 

##Build distribution

Run `$ npm run build` to create a `dist` folder and a production bundle to upload to your server. Your server configuration should point all the requests to the ìndex.html` file found there to make it work.


##Technologies
Slingshot offers a rich development experience using the following technologies:

| **Tech** | **Description** |**Learn More**|
|----------|-------|---|
|  [React](https://facebook.github.io/react/)  |   Fast, composable client-side components.    | [Pluralsight Course](https://www.pluralsight.com/courses/react-flux-building-applications)  |
|  [Redux](http://redux.js.org) |  Enforces unidirectional data flows and immutable, hot reloadable store. Supports time-travel debugging.| [Pluralsight Course](http://www.pluralsight.com/courses/react-redux-react-router-es6)    |
|  [Redux-Thunk](https://github.com/gaearon/redux-thunk) |  Redux Thunk middleware allows you to write action creators that return a function instead of an action.|  [Good read @ StackOverflow](http://stackoverflow.com/questions/35411423/how-to-dispatch-a-redux-action-with-a-timeout/35415559#35415559) |
|  [React Router](https://github.com/reactjs/react-router) | A complete routing library for React | [Pluralsight Course](https://www.pluralsight.com/courses/react-flux-building-applications) |
|  [Babel](http://babeljs.io) |  Compiles ES6 to ES5. Enjoy the new version of JavaScript today.     | [ES6 REPL](https://babeljs.io/repl/), [ES6 vs ES5](http://es6-features.org), [ES6 Katas](http://es6katas.org), [Pluralsight course](https://www.pluralsight.com/courses/javascript-fundamentals-es6)    |
| [Webpack](http://webpack.github.io) | Bundles npm packages and our JS into a single file. Includes hot reloading via [react-transform-hmr](https://www.npmjs.com/package/react-transform-hmr). | [Quick Webpack How-to](https://github.com/petehunt/webpack-howto) [Pluralsight Course](https://www.pluralsight.com/courses/webpack-fundamentals)|
| [Browsersync](https://www.browsersync.io/) | Lightweight development HTTP server that supports synchronized testing and debugging on multiple devices. | [Intro vid](https://www.youtube.com/watch?time_continue=1&v=heNWfzc7ufQ)|
| [Mocha](http://mochajs.org) | Automated tests with [Chai](http://chaijs.com/) for assertions and [Enzyme](https://github.com/airbnb/enzyme) for DOM testing without a browser using Node. | [Pluralsight Course](https://www.pluralsight.com/courses/testing-javascript) |
| [Isparta](https://github.com/douglasduteil/isparta) | Code coverage tool for ES6 code transpiled by Babel. | 
| [ESLint](http://eslint.org/)| Lint JS. Reports syntax and style issues. Using [eslint-plugin-react](https://github.com/yannickcr/eslint-plugin-react) for additional React specific linting rules. | |
| [SASS](http://sass-lang.com/) | Compiled CSS styles with variables, functions, and more. | [Pluralsight Course](https://www.pluralsight.com/courses/better-css)|
| [Editor Config](http://editorconfig.org) | Enforce consistent editor settings (spaces vs tabs, etc). | [IDE Plugins](http://editorconfig.org/#download) |
| [npm Scripts](https://docs.npmjs.com/misc/scripts)| Glues all this together in a handy automated build. | [Pluralsight course](https://www.pluralsight.com/courses/npm-build-tool-introduction), [Why not Gulp?](https://medium.com/@housecor/why-i-left-gulp-and-grunt-for-npm-scripts-3d6853dd22b8#.vtaziro8n)  |

##Other Modules
We are using a growing collection of npm modules into our apps. Here is a list of those modules and what they aim to do

| **Module** | **Description** | **url** |
|----------|-------|---|
| axios | Promise based HTTP client for the browser and node.js | https://github.com/mzabriskie/axios |
| classnames | Conditionally join classes onto classNames | https://github.com/JedWatson/classnames |
| jwt-decode | Decode JWT tokens | https://github.com/auth0/jwt-decode |
| lodash | JavaScript utility library delivering modularity, performance & extras | https://lodash.com/ |
| Redux Form | A Higher Order Component using react-redux to keep form state in a Redux store | https://github.com/erikras/redux-form |
| SkyLight | Simple react component for modals and dialogs | https://marcio.github.io/react-skylight/ |
| Rechart | Charting component for React | https://github.com/recharts/recharts |

##Translations
We are using [L10ns](http://l10ns.org/docs.html) as a tool for both translations and localization.

So please, install the global package:
```
$ npm install l10ns -g
```

Follow the instructions at [localizations.md](./src/localizations/localizations.md) to set everything up and start using L10ns on your environment.

##Techinal Debt
Make sure you add or delete items to our [TECH DEBT](./TECHDEBT.md) list accordingly for OS Backoffice & Results
